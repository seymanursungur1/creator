import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {API_BASE_URL} from '../../assets/constants';
import { Chapter } from '../_models/chapter';
@Injectable({
  providedIn: 'root'
})
export class ChapterService {

  private url = API_BASE_URL ;

  constructor(private httpClient: HttpClient) { }


  /*/All*/
  public getChapterByExperience(uuid: string){
    return this.httpClient.get(this.url + 'Chapter/GetChapterByExperience/' + uuid);
  }
  /*/{uuid}*/
  public getChapterById(uuid: string): Observable<any> {
    return this.httpClient.get(this.url + 'Chapter/' + uuid);
  }

  /*/Update*/
  public editChapter( uuid, data): Observable<any>{
    return this.httpClient.put(`${this.url}Chapter/Update`, data);
    }
  /*/Delete/{uuid}*/
    public deleteChapter(uuid): Observable<any>{
     return this.httpClient.delete(`${this.url}Chapter/Delete/${uuid}`);
    }
}
