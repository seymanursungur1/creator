import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpRequest} from '@angular/common/http';
import {API_BASE_URL} from '../../assets/constants';
import { Observable } from 'rxjs';
import { User } from '../_models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {


  private url = API_BASE_URL ;
  constructor(private httpClient: HttpClient) { }

  getAll() {
    return this.httpClient.get<User[]>(this.url + 'Users/All');
}
  /*/All*/
  public getAllUsers(){
    return this.httpClient.get(this.url + 'Users/All');
  }
/*/{uuid}*/
  public getUserByID(uuid: string): Observable<any> {
    return this.httpClient.get(this.url + 'Users/' + uuid);
  }
/*/All*/
  public addUser(data): Observable<any>{
  return this.httpClient.post(this.url + 'Users/Add', data);
  }
/*/Update*/
  public editUser( uuid, data): Observable<any>{
  return this.httpClient.put(`${this.url}Users/Update`, data);
  }
/*/Delete/{uuid}*/
  public deleteUser(uuid): Observable<any>{
   return this.httpClient.delete(`${this.url}Users/Delete/${uuid}`);
  }


  public upload(id: string, fileToUpload: File){
  const formData = new FormData();
  var extension = fileToUpload.name.split('?')[0].split('.').pop();
  formData.append('file', fileToUpload, id + "." + extension);
  console.log(formData);
  return this.httpClient.post(`${this.url}Users/UploadProfilePicture`, formData, {reportProgress: true, observe: 'events'});
}


}
