import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpRequest} from '@angular/common/http';
import {API_BASE_URL} from '../../assets/constants';
import { Observable } from 'rxjs';
import { User } from '../_models/user';

@Injectable({
  providedIn: 'root'
})
export class AnswerService {


  private url = API_BASE_URL ;
  constructor(private httpClient: HttpClient) { }

/*/{uuid}*/
  public getAnswerById(uuid: string): Observable<any> {
    return this.httpClient.get(this.url + 'Answer/' + uuid);
  }
  /*/{uuid}*/
  public getAnswerByQuestionId(uuid: string): Observable<any> {
    return this.httpClient.get(this.url + 'Answer/GetAnswersByQuestionId/' + uuid);
  }
/*/Update*/
  public editAnswer( uuid, data): Observable<any>{
  return this.httpClient.put(`${this.url}Answer/Update`, data);
  }
/*/Delete/{uuid}*/
  public deleteAnswer(uuid): Observable<any>{
   return this.httpClient.delete(`${this.url}Answer/Delete/${uuid}`);
  }

}
