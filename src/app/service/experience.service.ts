import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {API_BASE_URL} from '../../assets/constants';
import { Experience } from '../_models/experience';

@Injectable({
  providedIn: 'root'
})
export class ExperienceService {

  private url = API_BASE_URL ;

  constructor(private httpClient: HttpClient) { }

  // Overview
  getAll() {
    return this.httpClient.get<Experience[]>(this.url + 'Experience/Overview');
}
  /*/All*/
  public getAllExperiences(){
    return this.httpClient.get(this.url + 'Experience/All');
  }
/*/{uuid}*/
  public getExperienceByID(uuid: string): Observable<any> {
    return this.httpClient.get(this.url + 'Experience/' + uuid);
  }
/*/Add*/
public addExperience(data): Observable<any>{
  return this.httpClient.post(this.url + 'Experience/Add', data);
  }
/*/Update*/
  public editExperience( uuid, data): Observable<any>{
  return this.httpClient.put(`${this.url}Experience/Update`, data);
  }
/*/Delete/{uuid}*/
  public deleteExperience(uuid): Observable<any>{
   return this.httpClient.delete(`${this.url}Experience/Delete/${uuid}`);
  }
}
