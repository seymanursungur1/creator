import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpRequest} from '@angular/common/http';
import {API_BASE_URL} from '../../assets/constants';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CommentService {


  private url = API_BASE_URL ;
  constructor(private httpClient: HttpClient) { }


/*/{uuid}*/
  public getCommentByID(uuid: string): Observable<any> {
    return this.httpClient.get(this.url + 'Comment/' + uuid);
  }

  public getCommentByChapterID(uuid: string): Observable<any> {
    return this.httpClient.get(this.url + 'Comment/GetCommentsByChapterId/' + uuid);
  }

/*/All*/
  public addComment(data): Observable<any>{
  return this.httpClient.post(this.url + 'Comment/Add', data);
  }

  /*/All*/
  public addCommentByChapterId(uuid, data): Observable<any>{
    return this.httpClient.post(this.url + 'Comment/Add/'+ uuid, data);
    }

/*/Update*/
  public editComment( uuid, data): Observable<any>{
  return this.httpClient.put(`${this.url}Comment/Update`, data);
  }
/*/Delete/{uuid}*/
  public deleteComment(uuid): Observable<any>{
   return this.httpClient.delete(`${this.url}Comment/Delete/${uuid}`);
  }


}
