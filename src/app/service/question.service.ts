import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpRequest} from '@angular/common/http';
import {API_BASE_URL} from '../../assets/constants';
import { Observable } from 'rxjs';
import { User } from '../_models/user';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {


  private url = API_BASE_URL ;
  constructor(private httpClient: HttpClient) { }

/*/{uuid}*/
  public getQuestionById(uuid: string): Observable<any> {
    return this.httpClient.get(this.url + 'Question/' + uuid);
  }
  /*/{uuid}*/
  public getQuestionByQuizId(uuid: string): Observable<any> {
    return this.httpClient.get(this.url + 'Question/GetQuestionsByQuiz/' + uuid);
  }
/*/Update*/
  public editQuestion( uuid, data): Observable<any>{
  return this.httpClient.put(`${this.url}Question/Update`, data);
  }
/*/Delete/{uuid}*/
  public deleteQuestion(uuid): Observable<any>{
   return this.httpClient.delete(`${this.url}Question/Delete/${uuid}`);
  }

}
