import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { map , catchError} from 'rxjs/operators';
import { User } from '../_models/user';
import {API_BASE_URL} from '../../assets/constants';


@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;
    headers = new HttpHeaders().set('Content-Type', 'application/json');
    private url = API_BASE_URL ;

    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(username: string, password: string) {
        return this.http.post<any>(this.url + "Users/Authenticate", { username, password })
            .pipe(map(user => {
                localStorage.setItem('currentUser', JSON.stringify(user));
                this.currentUserSubject.next(user);
                return user;
            }));
    }

    logout() {
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }

    getAll() {
      return this.http.get<User[]>(this.url + 'Users/All');
  }

    getById(id: string) {
      return this.http.get<User>(this.url + 'Users/'+id);
  }

    public get loggedIn(): boolean{
    return localStorage.getItem('access_token') !==  null;
  }
}

