import { Injectable } from '@angular/core';
import * as AWS from 'aws-sdk/global';
import * as S3 from 'aws-sdk/clients/s3';
import 'rxjs/add/observable/of';
import { FileUpload } from '../_models/file-upload';
import { Observable } from 'rxjs/Observable';



@Injectable()
export class UploadFileService {

  FOLDER = 'Media/ProfilePictures';
  BUCKET = 'be.soulmade.genie';

  constructor() { }

  private getS3Bucket(): any {
    const bucket = new S3(
      {
        region: 'eu-west-3' ,
        accessKeyId: 'AKIA4YM57QNA5VEAS4L4',
            secretAccessKey: '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',

      }
    );

    return bucket;
  }

  uploadfile(file) {
    const params = {
      Bucket: this.BUCKET,
      Key: this.FOLDER + file.name,
      Body: file,
      ACL: 'public-read'
    };

    this.getS3Bucket().upload(params, function (err, data) {
      if (err) {
        console.log('There was an error uploading your file: ', err);
        return false;
      }

      console.log('Successfully uploaded file.', data);
      return true;
    });
  }

  getFiles(): Observable<Array<FileUpload>> {
    const fileUploads = new Array<FileUpload>();

    const params = {
      Bucket: this.BUCKET,
      Prefix: this.FOLDER
    };

    this.getS3Bucket().listObjects(params, function (err, data) {
      if (err) {
        console.log('There was an error getting your files: ' + err);
        return;
      }

      console.log('Successfully get files.', data);

      const fileDatas = data.Contents;

      /*fileDatas.forEach(function (file) {
        fileUploads.push(new FileUpload(file.Key, 'https://s3.amazonaws.com/' + params.Bucket + '/' + file.Key))
      });*/
      fileDatas.forEach(function (img){
        fileUploads.push(new FileUpload(img.Key, 'https://s3.eu-west-3.amazonaws.com/' + params.Bucket + '/' + img.Key))
      });
    });

    console.log(fileUploads);
    return Observable.of(fileUploads);
  }


}
