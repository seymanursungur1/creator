import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Content } from '../_models/content';
import {API_BASE_URL} from '../../assets/constants';
import * as AWS from 'aws-sdk/global';
import { S3 } from 'aws-sdk';

@Injectable({
  providedIn: 'root'
})
export class ContentService {

  private url = API_BASE_URL ;
  FOLDER = 'Media/Content/';
  BUCKET = 'be.soulmade.genie';

  constructor(private httpClient: HttpClient) { }

  private getS3Bucket(): any {
    const bucket = new S3(
      {
        region: 'eu-west-3' ,
        accessKeyId: 'AKIA4YM57QNA5VEAS4L4',
        secretAccessKey: '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',

      }
    );

    return bucket;
  }


  // Overview
  getAll() {
    return this.httpClient.get<Content[]>(this.url + 'Content/All/Overview');
}
  /*/All*/
  public getAllContents(){
    return this.httpClient.get(this.url + 'Content/All');
  }
  /*/{uuid}*/
  public getContentByID(uuid: string): Observable<any> {
    return this.httpClient.get(this.url + 'Content/' + uuid);
  }
/*/{uuid}*/
  public getContentByItemID(uuid: string): Observable<any> {
    return this.httpClient.get(this.url + 'Content/All/' + uuid);
  }
    /*/All*/
    public getLanguages(){
      return this.httpClient.get(this.url + 'Content/Languages');
    }
      /*/All*/
  public getQualities(){
    return this.httpClient.get(this.url + 'Content/Qualities');
  }
/*/Add*/
public addContent(data): Observable<any>{
  return this.httpClient.post(this.url + 'Content', data);
  }
  /*/Add*/
public addContentByItemId(itemId: string, data): Observable<any>{
  return this.httpClient.post(this.url + 'Content/Add/' + itemId, data);
  }
/*/Update*/
  public editContent( uuid, data): Observable<any>{
  return this.httpClient.put(`${this.url}Content/Update`, data);
  }

/*/Delete/{uuid}*/
  public deleteContent(uuid): Observable<any>{

   return this.httpClient.delete(`${this.url}Content/${uuid}`);

  }

  public deleteFromAmazone (id: string, uuid:string, fileToUpload: File){
    const params = {Key: this.FOLDER + uuid + '/' + id + "."+ extension, Body: fileToUpload, Bucket: this.BUCKET};

    var extension = fileToUpload.name.split('?')[0].split('.').pop();
    this.getS3Bucket().deleteObject(params, function(err, data) {
      if (err) { console.log(err) }
      else { console.log("Successfully deleted myBucket/myKey"); }
  });
  }

  public upload(id: string, uuid:string, fileToUpload: File){

    const formData = new FormData();
    var extension = fileToUpload.name.split('?')[0].split('.').pop();
    formData.append('file', fileToUpload, id + "." + extension);

    const params = {Key: this.FOLDER + uuid + '/' + id + "."+ extension, Body: fileToUpload, Bucket: this.BUCKET};

    this.getS3Bucket().upload(params, function (err, data) {
      if (err) {
        console.log('There was an error uploading your file: ', err);
        return false;
      }
      else{
      console.log('Successfully uploaded file.', data);
      return true;
      }
    });
    return this.httpClient.post(`${this.url}Content/Upload`, formData, {reportProgress: true, observe: 'events'});
  }

}
