import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpRequest} from '@angular/common/http';
import {API_BASE_URL} from '../../assets/constants';
import { Observable } from 'rxjs';
import { User } from '../_models/user';

@Injectable({
  providedIn: 'root'
})
export class ChapterOptionService {


  private url = API_BASE_URL ;
  constructor(private httpClient: HttpClient) { }

/*/{uuid}*/
public getChapterOptionByID(uuid: string): Observable<any> {
  return this.httpClient.get(this.url + 'ChapterOption/' + uuid);
}
/*/{uuid}*/
  public getChapterOptionByChapterID(uuid: string): Observable<any> {
    return this.httpClient.get(this.url + 'ChapterOption/GetChapterOptionsByChapterId/' + uuid);
  }

/*/Update*/
  public editChapterOption( uuid, data): Observable<any>{
  return this.httpClient.put(`${this.url}ChapterOption/Update`, data);
  }
/*/Delete/{uuid}*/
  public deleteChapterOption(uuid): Observable<any>{
   return this.httpClient.delete(`${this.url}ChapterOption/Delete/${uuid}`);
  }


}
