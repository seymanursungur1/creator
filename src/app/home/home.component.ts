import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
import { AuthenticationService } from '../service/authentication.service';
import { User } from '../_models/user';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
currentUser: User;

  constructor( public authService: AuthenticationService) {
      this.currentUser = this.authService.currentUserValue;
   }

  ngOnInit(): void {

  }
}


