import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { ExperienceService } from 'src/app/service/experience.service';
import { User } from 'src/app/_models/user';
import { Experience } from 'src/app/_models/experience';
import { Content } from 'src/app/_models/content';
import { Chapter } from 'src/app/_models/chapter';
import { first } from 'rxjs/operators';
import { ContentType } from 'src/app/_models/enums/content-type.enum';
import { VisibilityType } from 'src/app/_models/enums/visibility-type.enum';
@Component({
  selector: 'app-add-experience',
  templateUrl: './add-experience.component.html',
  styleUrls: ['./add-experience.component.css']
})
export class AddExperienceComponent implements OnInit {

  searchText;
  currentUser: User;
  //experience: Experience;
  chapters: Chapter[] = [];
  contents: Content[] = [];
  experiences= [];
  experience = {
    alias: '',
    version: '',
    gazeTime: '',
    reticleVisibile: false,
    content: [],
    chapters: []
  };

  constructor(private authService: AuthenticationService,
              private router: Router,
              private experienceService: ExperienceService,
              private http: HttpClient) {
    this.currentUser = this.authService.currentUserValue;
   }

  ngOnInit(): void {

    this.experienceService.getAllExperiences().subscribe((data: any[]) => {
      console.log(data);
      this.experiences = data;
    });
    console.log(this.experience);
  }


  addExperience(): void {
    const addExperienceData = {
    alias: this.experience.alias,
    version: this.experience.version,
    gazeTime: this.experience.gazeTime,
    reticleVisibile: this.experience.reticleVisibile,
    content: this.experience.content,
    chapters: this.experience.chapters,
    };

    /*addExperienceData.content = new Array<Content>();

    var titleContent = new Content();
    titleContent.alias = '';
    titleContent.key = '';
    titleContent.version = 0;
    titleContent.value = '';
    titleContent.quality = '';
    titleContent.type = ContentType[''];
    titleContent.language = '';
    titleContent.visibility = VisibilityType[''];

    addExperienceData.content.push(titleContent);
*/
    console.log(addExperienceData);
    this.experienceService.addExperience(addExperienceData)
    .pipe(first())
    .subscribe(experience => {
      this.router.navigate(['/list-experience'])},
      error => {
        console.log(error);
      }
    );
}


 /* addExperience(){
    this.experienceService.addExperience(this.experience)
    .pipe(first())
    .subscribe(experience => {
      this.router.navigate(['/list-experience'])},
      error =>{
        console.log(error);
      }
    );
  }*/
  logout(){
    this.authService.logout();
    this.router.navigate(['/login']);
  }
}
