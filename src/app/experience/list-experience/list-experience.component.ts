import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { ExperienceService } from 'src/app/service/experience.service';
import { Experience } from 'src/app/_models/experience';
import { User } from 'src/app/_models/user';

@Component({
  selector: 'app-list-experience',
  templateUrl: './list-experience.component.html',
  styleUrls: ['./list-experience.component.css']
})
export class ListExperienceComponent implements OnInit {

  contents = [];
  experiences = [];
  currentUser: User;
  currentExperience: Experience;

  constructor(private authService: AuthenticationService,
              private router: Router,
              private experienceService: ExperienceService,
              private http: HttpClient) {
    this.currentUser = this.authService.currentUserValue;
   }
  ngOnInit(): void {
    this.experienceService.getAllExperiences().subscribe((data: any[]) => {
      console.log(data);
      this.experiences = data;
    });


  }

  editExperience(uuid): void{
    this.router.navigate(['/edit-experience', uuid]);
  }

  detailExperience(uuid): void{
    this.router.navigate(['detail-experience/', uuid]);
  }
deleteExperience(uuid: string): void{
  this.experienceService.deleteExperience(uuid)
  .subscribe(
  response => {
    console.log(response);
    console.log(this.currentExperience);
    window.location.reload();
  },
  error => {
  console.error();
  });
  }


  logout(){
    this.authService.logout();
    this.router.navigate(['/login']);
  }

}
