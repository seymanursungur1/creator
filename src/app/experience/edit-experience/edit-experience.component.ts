import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AddChapterComponent } from 'src/app/chapter/add-chapter/add-chapter.component';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { ContentService } from 'src/app/service/content.service';
import { ExperienceService } from 'src/app/service/experience.service';
import { Chapter } from 'src/app/_models/chapter';
import { Experience } from 'src/app/_models/experience';
import { User } from 'src/app/_models/user';

@Component({
  selector: 'app-edit-experience',
  templateUrl: './edit-experience.component.html',
  styleUrls: ['./edit-experience.component.css']
})
export class EditExperienceComponent implements OnInit {


  currentUser: User;
  contents = [];
  uuid: string;
  currentExperience: Experience;
  editExperienceForm: FormGroup;
  selectedExperience: Experience;
  chapters = [];
  constructor(private authService: AuthenticationService,
              private router: Router,
              private formBuilder: FormBuilder,
              private experienceService: ExperienceService,
              private route: ActivatedRoute,
              private contentService: ContentService

              ) {this.currentUser = this.authService.currentUserValue;
              this.currentExperience = new Experience();}

  ngOnInit(): void {

    this.uuid = this.route.snapshot.paramMap.get('uuid');
    console.log(this.uuid);
    this.getExperience(this.uuid);
    console.log(this.selectedExperience);

    /*

    this.editExperienceForm = this.formBuilder.group({
      alias: ['', Validators.required],
      version: ['', Validators.required],
      gazeTime: ['', Validators.required],
      reticleVisibile: [false, Validators.required],
      content: [[], Validators.required],
      chapters: [[], Validators.required]
    });*/

  }


 getExperience(uuid: string): void{
    this.experienceService.getExperienceByID(uuid)
    .subscribe(
      data => {
        this.currentExperience = data;
        console.log(data);
        console.log(this.currentExperience.chapters);
        this.chapters = this.currentExperience.chapters;
        console.log(this.chapters);
        this.contents = this.currentExperience.content;
      },
      error => {
        console.log(error);
      }
    );
  }
  deleteExperience(uuid: string): void{
    this.experienceService.deleteExperience(uuid)
    .subscribe(
    response => {
      console.log(response);
      console.log(this.currentExperience);
      window.location.reload();
    },
    error => {
    console.error();
    });
    }
  detailChapter(uuidExperience, uuidChapter): void{
    this.router.navigate(['detail-chapter/', uuidExperience, uuidChapter]);
  }
  addChapter(uuidExperience): void{
    this.router.navigate(['add-chapter/', uuidExperience]);
  }
  deleteContent(uuid: string): void{
    this.contentService.deleteContent(uuid)
    .subscribe(
    response => {
      console.log(response);
      window.location.reload();
    },
    error => {
    console.error();
    });
    }
  detailContent(uuid): void{
    this.router.navigate(['detail-content/', uuid]);
  }


  /*updatePublished(): void{
    const editExperienceData = {
      alias: this.currentExperience.alias,
      version: this.currentExperience.version,
      gazeTime: this.currentExperience.gazeTime,
      reticleVisibile: this.currentExperience.reticleVisibile,
      content: this.currentExperience.content,
      chapters: this.currentExperience.chapters,
      };

    this.experienceService.editExperience(this.currentExperience.uuid, editExperienceData)
      .subscribe(
        res => {
          console.log(res);
           window.location.reload();
        },
        error => {
          console.log(error);
        }
      )
  }*/


  editExperience(): void {

    this.experienceService.editExperience(this.currentExperience.uuid, this.currentExperience)
    .pipe(first())
    .subscribe(res => {
         console.log(res);
         window.location.reload();
        },    error => {
          console.log(error);
        });
  }

  logout(){
    this.authService.logout();
    this.router.navigate(['/login']);
  }
}
