import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { ContentService } from 'src/app/service/content.service';
import { Content } from 'src/app/_models/content';
import { ContentType } from 'src/app/_models/enums/content-type.enum';
import { VisibilityType } from 'src/app/_models/enums/visibility-type.enum';
import { User } from 'src/app/_models/user';

@Component({
  selector: 'app-detail-content',
  templateUrl: './detail-content.component.html',
  styleUrls: ['./detail-content.component.css']
})
export class DetailContentComponent implements OnInit {

  currentContent: Content;
  currentUser: User;
  uuid: string;
  contents = [];
  visibilityTypes = VisibilityType;
  contentTypes = ContentType;
  qualities = [];

  constructor(private contentService: ContentService,
              private route: ActivatedRoute,
              private router: Router,
              private authService: AuthenticationService) {
              this.currentUser = this.authService.currentUserValue;
              }

  ngOnInit(): void {
    this.uuid = this.route.snapshot.paramMap.get('uuid');
    console.log(this.uuid);
    this.getContent(this.uuid);

      // All qualities
      this.contentService.getQualities().subscribe((data: any[]) => {
        this.qualities = data;
      });

  }

  getContent(uuid: string): void{
    this.contentService.getContentByID(uuid)
    .subscribe(
      data => {
        this.currentContent = data;
        console.log(data);

      },
      error => {
        console.log(error);
      }
    );
  }

  onSelectType(event){
    this.currentContent.type = event.target.value;
}
  onSelectLanguage(event){
    this.currentContent.language = event.target.value;
  }
  onSelectQuality(event){
    this.currentContent.quality = event.target.value;
  }
  onSelectVisibility(event){
    this.currentContent.visibility = event.target.value;
  }
  logout(){
    this.authService.logout();
    this.router.navigate(['/login']);
  }

  updatePublished(): void{
    const editContentData = {
      key: this.currentContent.key,
      version: this.currentContent.version,
      alias: this.currentContent.alias,
      value: this.currentContent.value,
      type: this.currentContent.type,
      quality: this.currentContent.quality,
      language: this.currentContent.language,
      visibility: this.currentContent.visibility
      };

    this.contentService.editContent(this.currentContent.uuid, editContentData)
      .subscribe(
        res => {
          console.log(res);
        },
        error => {
          console.log(error);
        }
      )
  }
  editContent(): void {
    this.contentService.editContent(this.currentContent.uuid, this.currentContent)
    .pipe(first())
    .subscribe(res => {
         console.log(res);
         window.location.reload();
        },    error => {
          console.log(error);
        });
  }


}
