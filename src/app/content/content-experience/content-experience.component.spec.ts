import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentExperienceComponent } from './content-experience.component';

describe('ContentExperienceComponent', () => {
  let component: ContentExperienceComponent;
  let fixture: ComponentFixture<ContentExperienceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentExperienceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentExperienceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
