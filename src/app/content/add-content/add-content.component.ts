import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AnswerService } from 'src/app/service/answer.service';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { ChapterOptionService } from 'src/app/service/chapter-option.service';
import { ChapterService } from 'src/app/service/chapter.service';
import { ContentService } from 'src/app/service/content.service';
import { ExperienceService } from 'src/app/service/experience.service';
import { QuestionService } from 'src/app/service/question.service';
import { Answer } from 'src/app/_models/answer';
import { Chapter } from 'src/app/_models/chapter';
import { ChapterOption } from 'src/app/_models/chapter-option';
import { Content } from 'src/app/_models/content';
import { ChapterOptionType } from 'src/app/_models/enums/chapter-option-type.enum';
import { ContentType } from 'src/app/_models/enums/content-type.enum';
import { VisibilityType } from 'src/app/_models/enums/visibility-type.enum';
import { Experience } from 'src/app/_models/experience';
import { User } from 'src/app/_models/user';
import { VariantSet } from 'src/app/_models/variantSet';

@Component({
  selector: 'app-add-content',
  templateUrl: './add-content.component.html',
  styleUrls: ['./add-content.component.css']
})
export class AddContentComponent implements OnInit {

  chapterSelection = false;
  chapterOptionSelection = false;
  commentSelection = false;
  questionSelection= false;
  variantSelection= false;
  answerSelection = false;
  selectedOptionType: string;

  selectedItemUuidToAddContent: string;
  answerContents = [];
  variantSet: VariantSet;
  currentContent: Content;
  selectedContentUuid: string;
  typeContent: string; // returns selected type as string
  visibilityContent: string;
  languageContent: string;
  qualityContent: string;
  optionType = ChapterOptionType;
  contentTypes = ContentType;
  visibilityTypes = VisibilityType;
  selectedFile: File = null;
  experiences: Experience[];
  currentExperience: Experience;
  selectedChapter: Chapter;
  selectedChapterOptionUuid: string;
  selectedQuestionUuid:string;
  selectedAnswerUuid:string;
  currentUser: User;
  url: any;
  show = false;
  showTextBox = false;
  uuidExperience: string;
  uuidChapter: string;
  uuidContent: string;
  experience = {
    uuid: '',
    alias: '',
    version: '',
    gazeTime: '',
    reticleVisibile: false,
    content: [],
    chapters: []
  };
  chapters = [];
  contents = [];
  variants = [];
  chapterOptions = [];
  comments = [];
  questions = [];
  answers : Answer;
  quizes = [];
  variantSets = [];
  languages = [];
  qualities = [];
  content = {
    key: '',
    version: 0,
    alias: '',
    type:  ContentType [''],
    value: '',
    quality: '',
    language: '',
    visibility: VisibilityType['']
  };
  textMode = true;
  uuid: string;
  selectedChapterOption: ChapterOption;
  constructor(private authService: AuthenticationService,
              private router: Router,
              private route: ActivatedRoute,
              private experienceService: ExperienceService,
              private chapterService: ChapterService,
              private contentService: ContentService,
              private chapterOptionService: ChapterOptionService,
              private questionService: QuestionService,
              private answerService: AnswerService
              ) {
    this.currentUser = this.authService.currentUserValue;
    this.currentExperience = new Experience();
    this.currentContent = new Content();
    this.selectedChapterOption = new ChapterOption();
   }

  ngOnInit(): void {

    // All experiences
    this.experienceService.getAllExperiences().subscribe((data: any[]) => {
     // console.log(data);
      this.experiences = data;
    });

    // All contents
    this.contentService.getAllContents().subscribe((data: any[]) => {
      this.contents = data;
    });

    this.getContent(this.selectedItemUuidToAddContent);
    // All languages
    this.contentService.getLanguages().subscribe((data: any[]) => {
      this.languages = data;
    });
    // All qualities
    this.contentService.getQualities().subscribe((data: any[]) => {
      this.qualities = data;
    });

    console.log(this.selectedContentUuid);
    console.log(this.show);
    if(!this.selectedContentUuid == null){
      this.show = true;
      console.log(this.show);
    }
    else{
      this.show = false;
      console.log(this.show);
    }
  }

  onSelectExperience(event){
    this.experience.uuid = event.target.value;
    this.chapterSelection = true;
    this.chapterOptionSelection = false;
    this.commentSelection = false;
    this.questionSelection= false;
    this.variantSelection= false;
    this.answerSelection = false;
    console.log(event);
    this.experiences.filter(element => {
      if (element.uuid == event.target.value){
        this.uuidExperience = element.uuid;
        this.chapters = element.chapters;
        this.selectedItemUuidToAddContent = this.uuidExperience;
        console.log(this.selectedItemUuidToAddContent);
      }
    });

    this.contentService.getContentByItemID(this.selectedItemUuidToAddContent)
    .subscribe(
      data => {
        this.contents = data;
      },
      error => {
        console.log(error);
      }
    );
  }


  onSelectContent(event){
    this.selectedContentUuid = event.target.value;
    console.log(this.selectedContentUuid);

    this.show = true;
    console.log(this.show);
    this.contentService.getContentByID(this.selectedContentUuid)
    .subscribe(
      data => {
        this.currentContent = data;
        console.log(this.currentContent);
      },
      error => {
        console.log(error);
      }
    );
  }
  onSelectedChapterOption(event){

    this.questionSelection = false;
    this.variantSelection = false;
    this.chapterSelection = true;
    this.chapterOptionSelection = true;
    this.commentSelection = false;
    this.answerSelection = false;
    this.selectedChapterOptionUuid = event.target.value;
    console.log(this.selectedChapterOptionUuid);

    this.selectedItemUuidToAddContent = this.selectedChapterOptionUuid;
    console.log(this.selectedItemUuidToAddContent);
    this.show = false;
    console.log(this.show);
    this.chapterOptionService.getChapterOptionByID(this.selectedChapterOptionUuid)
    .subscribe(
      data => {
        this.selectedChapterOption = data;
        this.variantSet = data;
        console.log(this.variantSet);
        // Button
        if(this.selectedChapterOption.OptionType == 0){
          this.selectedChapterOption = data;
          this.questionSelection = true;
          this.variantSelection= false;
          this.questionService.getQuestionByQuizId(this.selectedItemUuidToAddContent)
          .subscribe(
            data => {
              this.questions = data;
              console.log(this.questions);
            },
            error =>{
              console.log(error);
            }
          );
        }
        //Quiz
       if(this.selectedChapterOption.OptionType == 1){
        this.variantSelection= false;
        this.questionSelection = false;

       }
       // VariantSet
       if(this.selectedChapterOption.OptionType == 2){
        this.variants = this.variantSet.Variants;
        console.log(this.variants);
        this.variantSelection= true;
        this.questionSelection = false;
       }
      }
      ,
      error =>{
        console.log(error);
      }
    );

    this.contentService.getContentByItemID(this.selectedItemUuidToAddContent)
    .subscribe(
      data => {
        this.contents = data;
        console.log(this.contents);
      },
      error => {
        console.log(error);
      }
    );
  }
  getContent(uuid: string): void{
    this.contentService.getContentByItemID(uuid)
    .subscribe(
      data => {
        this.contents = data;
        console.log(this.contents);
      },
      error => {
        console.log(error);
      }
    );
  }

  updatePublished(): void{
    const editContentData = {
      key: this.currentContent.key,
      version: this.currentContent.version,
      alias: this.currentContent.alias,
      value: this.currentContent.value,
      type: this.currentContent.type,
      quality: this.currentContent.quality,
      language: this.currentContent.language,
      visibility: this.currentContent.visibility
      };

    this.contentService.editContent(this.currentContent.uuid, editContentData)
      .subscribe(
        res => {
          console.log(res);
        },
        error => {
          console.log(error);
        }
      )
  }
  editContent(): void {
    this.contentService.editContent(this.currentContent.uuid, this.currentContent)
    .pipe(first())
    .subscribe(res => {
         console.log(res);
         window.location.reload();
        },    error => {
          console.log(error);
        });
  }

  onSelectQuestion(event){
    this.questionSelection = true;
    this.variantSelection = false;
    this.chapterSelection = true;
    this.chapterOptionSelection = true;
    this.commentSelection = false;
    this.answerSelection = true;
    this.selectedQuestionUuid = event.target.value;
    console.log(this.selectedQuestionUuid);

    this.selectedItemUuidToAddContent = this.selectedQuestionUuid;
    console.log(this.selectedItemUuidToAddContent);
    this.show = false;
    console.log(this.show);
    this.answerService.getAnswerByQuestionId(this.selectedItemUuidToAddContent)
    .subscribe(
      data => {
        this.answers = data;
        console.log(this.answers);
        this.answerContents = this.answers.content;
      },
      error =>{
        console.log(error);
      }
    );
    this.contentService.getContentByItemID(this.selectedItemUuidToAddContent)
    .subscribe(
      data => {
        this.contents = data;
      },
      error => {
        console.log(error);
      }
    );
  }

  onSelectedComment(event){
    this.selectedItemUuidToAddContent = event.target.value;
    console.log(this.selectedItemUuidToAddContent);
  }

  onSelectVariant(event){
    this.selectedItemUuidToAddContent = event.target.value;
    console.log(this.selectedItemUuidToAddContent);


    this.contentService.getContentByItemID(this.selectedItemUuidToAddContent)
    .subscribe(
      data => {
        this.contents = data;
        console.log(this.contents);
      },
      error => {
        console.log(error);
      }
    );
  }
  onSelectAnswer(event){
    this.selectedAnswerUuid = event.target.value;

    this.selectedItemUuidToAddContent = this.selectedAnswerUuid;
    this.show = false;
    console.log(this.show);
    this.contentService.getContentByItemID(this.selectedItemUuidToAddContent)
    .subscribe(
      data => {
        this.contents = data;
      },
      error => {
        console.log(error);
      }
    );
  }

  onSelectChapter(event){
    this.show = false;
    this.chapterSelection = true;
    this.chapterOptionSelection = true;
    this.commentSelection = true;
    this.questionSelection= false;
    this.variantSelection= false;
    this.answerSelection = false;
    console.log(this.show);
    this.chapters.filter(element => {
      // tslint:disable-next-line: triple-equals
      if (element.uuid == event.target.value){
           this.uuidChapter = element.uuid;
           this.selectedItemUuidToAddContent = this.uuidChapter;
           console.log(this.uuidChapter);

           this.chapterService.getChapterById(this.uuidChapter)
           .subscribe(
             data => {
               this.selectedChapter = data;
               this.chapterOptions = this.selectedChapter.chapterOptions;
              console.log(this.selectedChapter);
              this.comments = this.selectedChapter.comments;
              console.log(this.chapterOptions);
              console.log(this.comments);
             },
             error => {
               console.log(error);
             }
           );
      }
    });
    this.contentService.getContentByItemID(this.selectedItemUuidToAddContent)
    .subscribe(
      data => {
        this.contents = data;
      },
      error => {
        console.log(error);
      }
    );
  }


  textBoxActive(){
    console.log(this.typeContent);
    console.log(this.showTextBox);
    if (this.typeContent == '0'){
      console.log(this.showTextBox);
      this.showTextBox = !this.showTextBox;
    }
    else{
      this.showTextBox = false;
    }
  }
  createContent(): void{

    const addContentData = {
    key: this.content.key,
    version: this.content.version,
    alias: this.content.alias,
    value: this.content.value,
    type: this.content.type,
    quality: this.content.quality,
    language: this.content.language,
    visibility: this.content.visibility
    };
    // addContentData.type = this.typeContent;
    //addContentData.visibility = this.visibilityContent;
    addContentData.quality = this.qualityContent;
    addContentData.language = this.languageContent;

   /* var contentClass = new Content();
    contentClass.uuid  = this.uuidContent;

    if(this.selectedFile == null ){
      addContentData.value = this.content.value;
     // addContentData.value = null;
    }
    else {
      addContentData.value = this.selectedFile.name;
    }*/

    this.contentService.addContentByItemId(this.selectedItemUuidToAddContent, addContentData)

      .subscribe(data => {
        var cD = data as Content;
        console.log(data);
        console.log(this.selectedFile);
        if(this.typeContent == "0" || this.selectedFile == null){ // als text gekozen is en geen foto is, value is value
          console.log('you did it');
           // addContentData.value = this.content.value;
        }
        else{
          data.value = this.selectedFile.name;
          console.log("dedeefe");
          console.log(this.selectedFile);
          this.contentService.upload( data.uuid,this.selectedItemUuidToAddContent, this.selectedFile)
        .subscribe(res => {
          console.log(res);

          this.router.navigate(['/list-experience']);

      });
        }
      },
        error => {
          console.log(error);
      }
);

    console.log(this.content);
    console.log(addContentData);

  /*  console.log(this.selectedUuid);
    console.log(this.languageContent);
    console.log(this.typeContent);
    console.log(this.visibilityContent);
    console.log(this.qualityContent);*/
}



  onSelectType(event){
    this.content.type = event.target.value;
    this.typeContent = this.content.type;
    this.contents.filter(element => {
      if (element.type == event.target.value){
        this.typeContent = element.type;
      }
    });

}
  onSelectLanguage(event){
    this.content.language = event.target.value;
    this.contents.filter(element => {
      if (element.language == event.target.value){
        this.languageContent = element.language;
      }
    });
  }
  onSelectQuality(event){
    this.content.quality = event.target.value;
    this.contents.filter(element => {
      if (element.quality == event.target.value){
        this.qualityContent = element.quality;
      }
    });
  }
  onSelectVisibility(event){
    this.content.visibility = event.target.value;
   /* this.contents.filter(element =>{
      if(element.visibility == event.target.value){
        this.visibilityContent = element.visibility;
      }
      console.log(this.visibilityContent);
    });*/
    this.visibilityContent = this.content.visibility;
  }
  logout(){
    this.authService.logout();
    this.router.navigate(['/login']);
}
  onSelected(event) {
    this.selectedFile = (event.target.files[0] as File);
    const reader = new FileReader();
    reader.readAsDataURL(this.selectedFile);
    reader.onload = (_event) => {
      this.url = reader.result;
    };
    console.log(this.selectedFile);
    console.log(this.url);
}

public delete(){
  this.url = null;
  }

  deleteContent(uuid: string): void{
    this.contentService.deleteContent(uuid)
    .subscribe(
    response => {
      console.log(response);
      window.location.reload();
    },
    error => {
    console.error();
    });
  }
}
