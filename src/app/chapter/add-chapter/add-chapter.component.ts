import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { ExperienceService } from 'src/app/service/experience.service';
import { ButtonTeleport } from 'src/app/_models/button-teleport';
import { Chapter } from 'src/app/_models/chapter';
import { Content } from 'src/app/_models/content';
import { ButtonType } from 'src/app/_models/enums/button-type.enum';
import { ChapterOptionType } from 'src/app/_models/enums/chapter-option-type.enum';
import { ContentType } from 'src/app/_models/enums/content-type.enum';
import { TeleportTargetType } from 'src/app/_models/enums/teleport-target-type.enum';
import { VisibilityType } from 'src/app/_models/enums/visibility-type.enum';
import { Experience } from 'src/app/_models/experience';
import { Location } from 'src/app/_models/location';
import { Question } from 'src/app/_models/question';
import { Quiz } from 'src/app/_models/quiz';
import { Rotation } from 'src/app/_models/rotation';
import { User } from 'src/app/_models/user';

@Component({
  selector: 'app-add-chapter',
  templateUrl: './add-chapter.component.html',
  styleUrls: ['./add-chapter.component.css']
})
export class AddChapterComponent implements OnInit {

  uuid: string;
  currentUser: User;
  chapter = {
      alias: '',
      maxOpendHotspots : null,
      buttons: [],
      chapterOptions: [],
      comments: [],
      content: [],
      quizes: [],
      navigateOnCompletion : false,
      navigationMenuEnabled : false,
      order : null,
      pointerAllowed : false,
  };
  currentExperience: Experience;
  chapters = [];
  experience = {
      chapters: []
  };



  constructor(
              private experienceService: ExperienceService,
              private router: Router,
              private route: ActivatedRoute,
              private authService: AuthenticationService) {
                this.currentUser = this.authService.currentUserValue;
               }

  ngOnInit(): void {
    this.uuid = this.route.snapshot.paramMap.get('uuidExperience');

    console.log(this.uuid);
    this.getExperience(this.uuid);
  }
  logout(){
    this.authService.logout();
    this.router.navigate(['/login']);
  }

  getExperience(uuid: string): void{
    this.experienceService.getExperienceByID(uuid)
    .subscribe(
      data => {
        this.currentExperience = data;
        this.chapters = this.currentExperience.chapters;
        console.log(this.chapters);
      },
      error => {
        console.log(error);
      }
    );
  }

  addChapterByEditExperience(): void {
    const addChapterData = {
      alias: this.chapter.alias,
      buttons: this.chapter.buttons,
      chapterOptions: this.chapter.chapterOptions,
      comments: this.chapter.comments,
      content: this.chapter.content,
      quizes: this.chapter.quizes,
      maxOpendHotspots : this.chapter.maxOpendHotspots,
      navigateOnCompletion : this.chapter.navigateOnCompletion,
      navigationMenuEnabled : this.chapter.navigationMenuEnabled,
      order : this.chapter.order,
      pointerAllowed : this.chapter.pointerAllowed,
    };


    const addExperienceData = {
      chapters: this.experience.chapters,
    };

    addExperienceData.chapters = new Array<Chapter>();
    addExperienceData.chapters.push(addChapterData);

    this.currentExperience.chapters = addExperienceData.chapters;

    console.log("experienceData")
    console.log(addExperienceData);
    console.log(this.currentExperience.uuid);
    console.log(this.currentExperience);
    console.log(this.currentExperience.chapters);

    this.experienceService.editExperience(this.currentExperience.uuid, this.currentExperience)
    .pipe(first())
    .subscribe(res => {
         console.log(res);
        // window.location.reload();
        },    error => {
          console.log(error);
        });
}


}
