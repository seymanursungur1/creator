import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { ChapterOptionService } from 'src/app/service/chapter-option.service';
import { ChapterService } from 'src/app/service/chapter.service';
import { CommentService } from 'src/app/service/comment.service';
import { ContentService } from 'src/app/service/content.service';
import { ExperienceService } from 'src/app/service/experience.service';
import { Chapter } from 'src/app/_models/chapter';
import { Experience } from 'src/app/_models/experience';
import { User } from 'src/app/_models/user';

@Component({
  selector: 'app-detail-chapter',
  templateUrl: './detail-chapter.component.html',
  styleUrls: ['./detail-chapter.component.css']
})
export class DetailChapterComponent implements OnInit {

  searchText: string;
  currentUser: User;
  currentExperience: Experience;
  currentChapter: Chapter;
  uuidCurrentExperience: string;
  uuidCurrentChapter: string;
  chapters = [];
  chaptersExp = [];
  experiences = [];
  buttons = [];
  quizes = [];
  variantSets = [];
  contents = [];
  comments = [];
  chapterOptions = [];
  selectedChapter = [];


  /*  uuid: string;
  alias:string;
  order: number;
  maxOpendHotspots:number;
  navigationMenuEnabled:boolean;
  pointerAllowed:boolean;
  focusObjectType:string;
  content: Array<Content>;
  //chapterOptions : Array<ChapterOption>;
*/

  constructor(private chapterService: ChapterService,
              private route: ActivatedRoute ,
              private router: Router,
              private authService: AuthenticationService,
              private contentService: ContentService,
              private commentService: CommentService,
              private chapterOptionService: ChapterOptionService,
              private experienceService: ExperienceService) {
              this.currentUser = this.authService.currentUserValue;
              this.currentChapter = new Chapter();
              }


  ngOnInit(): void {


    this.uuidCurrentExperience = this.route.snapshot.paramMap.get('uuidExperience');
   // console.log(this.uuidCurrentExperience);
    this.uuidCurrentChapter = this.route.snapshot.paramMap.get('uuidChapter');
    //console.log(this.uuidCurrentChapter);
    this.getExperience(this.uuidCurrentExperience);
    this.getChapter(this.uuidCurrentChapter);
    this.getContent(this.uuidCurrentChapter);
    this.getComments(this.uuidCurrentChapter);
    this.getChapterOptions(this.uuidCurrentChapter);
  }


  editChapter(): void {
    this.chapterService.editChapter(this.currentChapter.uuid, this.currentChapter)
    .pipe(first())
    .subscribe(res => {
         console.log(res);
         console.log("gelukt");
         window.location.reload();
        },    error => {
          console.log(error);
        });
  }
  getExperience(uuid: string): void{
    this.experienceService.getExperienceByID(uuid)
    .subscribe(
      data => {
        this.currentExperience = data;
        this.chaptersExp = this.currentExperience.chapters;
        const result = this.chaptersExp.find(({uuid}) => uuid === this.uuidCurrentChapter);
        console.log(result);
        this.buttons = result.buttons;
        console.log(this.buttons);
        this.quizes = result.quizes;
        console.log(this.quizes);
        this.variantSets = result.variantSets;
        console.log(this.variantSets);
        this.comments = result.comments;
        console.log(this.comments);
      },
      error => {
        console.log(error);
      }
    );
  }


  getChapter(uuid: string): void{
    this.chapterService.getChapterById(uuid)
    .pipe(first())
    .subscribe(
      data => {
        this.currentChapter = data;

      },
      error => {
        console.log(error);
      }
    );
  }

  addChapterOptionButton(uuidExperience: string, uuidChapter: string){
    this.router.navigate(['add-chapterOption/', uuidExperience , uuidChapter]);

  }

  addButton(uuidExperience: string, uuidChapter: string){
    this.router.navigate(['add-button/', uuidExperience, uuidChapter]);
  }

  addQuiz(uuidExperience: string, uuidChapter: string){
    this.router.navigate(['add-quiz/', uuidExperience, uuidChapter]);
  }

  addVariantSet(uuidExperience: string, uuidChapter: string){
    this.router.navigate(['add-variant-set/', uuidExperience, uuidChapter]);
  }

  detailChapterOption(uuidChapterOption: string){
    this.router.navigate(['detail-chapterOption/' , uuidChapterOption]);

  }

  detailButton(uuidExperience: string, uuidChapter: string, uuidChapterOption: string){
    this.router.navigate(['detail-button/' , uuidExperience, uuidChapter, uuidChapterOption]);

  }
  detailQuiz(uuidChapterOption: string){
    this.router.navigate(['detail-quiz/' , uuidChapterOption]);

  }
  detailVariantSet(uuidChapter:string, uuidChapterOption: string){
    this.router.navigate(['detail-variantSet/' , uuidChapter, uuidChapterOption]);

  }
  addComment(uuidExperience: string, uuidChapter: string): void{
    this.router.navigate(['/add-comment', this.uuidCurrentExperience, this.uuidCurrentChapter]);
  }
  getContent(uuid: string): void{
    this.contentService.getContentByItemID(uuid)
    .subscribe(
      data => {
        this.contents = data;
       // console.log("contents:")
       // console.log(data);
      },
      error => {
        console.log(error);
      }
    );
  }

  getComments(uuid: string): void{
    this.commentService.getCommentByChapterID(uuid)
    .subscribe(
      data => {
        this.comments = data;
       // console.log("comments:");
       // console.log(data);
      },
      error => {
        console.log(error);
      }
    );
  }

  getChapterOptions(uuid: string): void{
    this.chapterOptionService.getChapterOptionByChapterID(uuid)
    .subscribe(
      data => {
        this.chapterOptions = data;
      //  console.log("chapterOptions:");
        //console.log(data);
      },
      error => {
        console.log(error);
      }
    );
  }

  detailContent(uuid): void{
    this.router.navigate(['detail-content/', uuid]);
  }

  deleteContent(uuid: string): void{
    this.contentService.deleteContent(uuid)
    .subscribe(
    response => {
      console.log(response);
      window.location.reload();
    },
    error => {
    console.error();
    });
    }
  logout(){
    this.authService.logout();
    this.router.navigate(['/login']);
  }

  detailComment(uuid):void{
    this.router.navigate(['detail-comment/', uuid]);
  }

  deleteComment(uuid:string): void{
    this.commentService.deleteComment(uuid)
    .subscribe(
    response => {
      console.log(response);
      window.location.reload();
    },
    error => {
    console.error();
    });
  }

  deleteChapter(uuid:string): void{
    this.chapterService.deleteChapter(uuid)
    .subscribe(
    response => {
      console.log(response);
      window.location.reload();
    },
    error => {
    console.error();
    });
  }

  deleteChapterOption(uuid: string): void{
    this.chapterOptionService.deleteChapterOption(uuid)
    .subscribe(
    response => {
      console.log(response);
      window.location.reload();
    },
    error => {
    console.error();
    });
  }
}
