import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { UploadFileService } from '../service/upload-file.service';
import { FileUpload } from '../_models/file-upload';


@Component({
  selector: 'app-list-upload',
  templateUrl: './list-upload.component.html',
  styleUrls: ['./list-upload.component.css']
})
export class ListUploadComponent implements OnInit {

  showFile = false;
  fileUploads: Observable<Array<FileUpload>>;
  fileUpload: FileUpload;

  constructor(private uploadService: UploadFileService) { }

  ngOnInit() {
  }

  showFiles(enable: boolean) {
    this.showFile = enable;

    if (enable) {
      this.fileUploads = this.uploadService.getFiles();

    }
  }

}
