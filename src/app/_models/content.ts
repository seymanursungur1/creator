
import { ContentType } from './enums/content-type.enum';
import { VisibilityType } from './enums/visibility-type.enum';


export class Content {
  uuid: string;
  key: string;
  version: number;
  alias: string;
  value: string;
  type: ContentType;
  quality: string;
  language: string;
  visibility: VisibilityType;

  constructor(){
  }
}
