import { ChapterOption } from './chapter-option';
import { Content } from './content';
import { ChapterOptionType } from './enums/chapter-option-type.enum';
import { VisibilityType } from './enums/visibility-type.enum';
import { Location } from './location';
import { Question } from './question';
import { Rotation } from './rotation';

export class Quiz {
  content: Content[];
  order: number;
  optionType: ChapterOptionType;
  uuid: string;
  alias: string;
  location: Location;
  questions: Array<Question>;
  autoOpen: boolean;
  visibility: VisibilityType;
  rotation: Rotation;

  constructor(){
    this.questions = new Array<Question>();
    this.optionType = ChapterOptionType.Quiz;
    this.content = new Array<Content>();
  }
}
