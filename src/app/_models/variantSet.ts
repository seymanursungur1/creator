import { ChapterOptionType } from './enums/chapter-option-type.enum';
import { VisibilityType } from './enums/visibility-type.enum';
import { Rotation } from './rotation';
import { Location } from './location';
import { Variant } from './variant';
import { Content } from './content';

export class VariantSet {
  Uuid: string;
  SetName: string;
  OptionType: ChapterOptionType;
  Variants: Array<Variant>;
  Order: number;
  Visibility: VisibilityType;
  Location: Location;
  Rotation: Rotation;
  Content: Array<Content>;
  Alias: string;

  constructor(){
    this.Content = new Array<Content>();
    this.Variants = new Array<Variant>();
    this.OptionType = ChapterOptionType.VariantSet;
  }
}
