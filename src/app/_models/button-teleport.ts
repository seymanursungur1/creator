import { TeleportTargetType } from './enums/teleport-target-type.enum';

export class ButtonTeleport {
  uuid: string;
  teleportTarget: string;
  targetType: TeleportTargetType;

  constructor(){}
}
