import { GenderType } from 'aws-sdk/clients/rekognition';

export class User {
  uuid: string;
  userName: string;
  password: string;
  firstName: string;
  lastName: string;
  gender: GenderType;
  profilePicture: string;
  hasProfilePicture: boolean;
  profileColor: string;
  email: string;
  token?: string;
}
