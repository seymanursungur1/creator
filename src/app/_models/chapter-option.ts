import { ButtonTeleport } from './button-teleport';
import { Content } from './content';
import { ButtonType } from './enums/button-type.enum';
import { ChapterOptionType } from './enums/chapter-option-type.enum';
import { VisibilityType } from './enums/visibility-type.enum';
import { Location } from './location';
import { Rotation } from './rotation';

export class ChapterOption {
  Uuid: string;
  Alias: string;
  AutoOpen: boolean;
  ButtonType: ButtonType;
  Content: Array<Content>;
  Location: Location;
  OptionType: ChapterOptionType;
  Order: number;
  Rotation: Rotation;
  Teleport: ButtonTeleport;
  Visibility: VisibilityType;


}
