import { Content } from './content';

export class Answer {
  uuid: string;
  order: number;
  alias: string;
  content: Array<Content>;
  correct: boolean;

  constructor(){
    this.content = new Array<Content>();
    
  }
}
