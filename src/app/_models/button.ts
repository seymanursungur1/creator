import { ButtonTeleport } from './button-teleport';
import { Content } from './content';
import { ButtonType } from './enums/button-type.enum';
import { ChapterOptionType } from './enums/chapter-option-type.enum';
import { VisibilityType } from './enums/visibility-type.enum';
import { Location } from './location';
import { Rotation } from './rotation';

export class Button {
  order: number;
  optionType: ChapterOptionType;
  uuid: string;
  alias: string;
  location: Location;
  autoOpen: boolean;
  buttonType: ButtonType;
  content: Array<Content>;
  rotation: Rotation;
  teleport: ButtonTeleport;
  visibility: VisibilityType;

  constructor(){
    this.content = new Array<Content>();
    this.optionType = ChapterOptionType.Button;
  }
}
