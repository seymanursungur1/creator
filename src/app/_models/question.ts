import { Answer } from 'aws-sdk/clients/kinesisvideosignalingchannels';
import { Content } from './content';

export class Question {
  uuid: string;
  order: number;
  alias: string;
  content: Array<Content>;
  answers: Array<Answer>;

  constructor(){
    this.content = new Array<Content>();
    this.answers = new Array<Answer>();
  }
}
