import { Button } from './button';
import { ChapterOption } from './chapter-option';
import { Content } from "./content";
import { Quiz } from './quiz';
import { VariantSet } from './variantSet';


export class Chapter {
  uuid: string;
  alias: string;
  buttons: Array<Button>;
  chapterOptions: Array<ChapterOption>;
  comments: Array<Comment>;
  content: Array<Content>;
  maxOpendHotspots: number;
  navigateOnCompletion: boolean;
  navigationMenuEnabled: boolean;
  order: number;
  pointerAllowed: boolean;
  quizes: Array<Quiz>;
  variantSets: Array<VariantSet>;


  constructor(){
    this.content = new Array<Content>();
    this.chapterOptions = new Array<ChapterOption>();
    this.buttons = new Array<Button>();
  }
}
