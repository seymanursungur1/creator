import { Content } from './content';
import { Location } from './location';
import { Rotation } from './rotation';
import { User } from './user';

export class Comment {
  uuid: string;
  author: User;
  createDate: string = new Date().toDateString();
  size: number;
  location: Location;
  rotation: Rotation;
  content: Array<Content>;
  alias: string;
}
