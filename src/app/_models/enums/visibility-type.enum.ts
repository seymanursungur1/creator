export enum VisibilityType{
  Visible = 0,
  Invisible = 1,
  Hidden = 2,
  OnlyController = 3,
  OnlyViewer = 4
}
