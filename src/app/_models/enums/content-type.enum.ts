export enum ContentType {
  Text = 0,
  Thumbnail = 1,
  Video = 2,
  Sound = 3,
  Photo = 4,
  Video_360 = 5,
  Audio_360 = 6,
  Photo_360 = 7,
  GenericObject = 8,
  UnityObject = 9,
  UnrealObject = 10,
  ForegroundColor = 11,
  BackgroundColor = 12,
  Icon = 13
}
