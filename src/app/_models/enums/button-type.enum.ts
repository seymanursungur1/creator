export enum ButtonType {
  Content = 0,
  Teleport = 1,
  ChangeVars = 2,
  Switch = 3
}
