import { Chapter } from './chapter';
import { Content } from './content';


export class Experience {
  uuid: string;
  alias: string;
  version: number;
  gazeTime: number;
  reticleVisibile: boolean;
  content: Array<Content>;
  chapters: Array<Chapter>;

  constructor(){
    this.reticleVisibile = false;
    this.chapters = new Array<Chapter>();
    this.content = new Array<Content>();
  }
}
