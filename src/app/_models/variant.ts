import { Content } from './content';

export class Variant {
  uuid: string;
  variantName: string;
  content: Array<Content>;
  alias: string;

  constructor(){
    this.content = new Array<Content>();
  }
}
