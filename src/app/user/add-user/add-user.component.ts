import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { UserService } from 'src/app/service/user.service';
import { User } from 'src/app/_models/user';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  user = {
    firstName: '',
    lastName: '',
    userName: '',
    password: '',
    email: '',
    hasProfilePicture: false,
    profilePicture: ''
  };
  profilePictureUrl: any;
  submitted: boolean;
  selectedFile: File = null;
  currentUser: User;
  toFile;


  constructor(private userService: UserService,
              private router: Router,
              private authService: AuthenticationService,
              private http: HttpClient) {
    this.currentUser = this.authService.currentUserValue;
   }

  ngOnInit(): void {
  }
  saveUser(): void {
    const addUserData = {
    firstName: this.user.firstName,
    lastName: this.user.lastName,
    userName: this.user.userName,
    password: this.user.password,
    email: this.user.email,
    hasProfilePicture: this.user.hasProfilePicture,
    profilePicture: this.user.profilePicture
    };


    if (this.selectedFile != null){
      addUserData.hasProfilePicture = true;
      addUserData.profilePicture = this.selectedFile.name;
  }


    this.userService.addUser(addUserData)
      .subscribe(
        data => {
          var userData = data as User;

          this.userService.upload(userData.uuid, this.selectedFile)
          .subscribe(res => {
            console.log(res);
            this.submitted = true;
            this.router.navigate(['/list-user']);
          }),
          error => {
          console.log(error);
        };
    });
}


  newUser(): void {
    this.submitted = false;
    this.user = {
    firstName: '',
    lastName: '',
    userName: '',
    password: '',
    email: '',
    hasProfilePicture: false,
    profilePicture: ''
    };
    
  }

  onSelected(event) {
    this.selectedFile = <File> event.target.files[0];
    var reader = new FileReader();
    reader.readAsDataURL(this.selectedFile);
    reader.onload = (_event) => {
      this.profilePictureUrl = reader.result;
    };
    console.log(this.selectedFile);
    console.log(this.profilePictureUrl);
  }


}


