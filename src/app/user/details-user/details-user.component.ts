import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { User } from 'src/app/_models/user';
import { UserService } from 'src/app/service/user.service';
import { GenderType } from 'src/app/_models/enums/gender-type.enum';


@Component({
  selector: 'app-details-user',
  templateUrl: './details-user.component.html',
  styleUrls: ['./details-user.component.css']
})
export class DetailsUserComponent implements OnInit {

editUserData: FormGroup;
genderTypes = GenderType;
currentUser: User;
selectedUser: User;
uuid: string;
disabled = true;
btn: boolean = false;
url: any;
selectedFile: File = null;

constructor( private userService: UserService,
             public dialogRef: MatDialogRef<DetailsUserComponent>,
             private route: ActivatedRoute,
             private formBuilder: FormBuilder,
             private router: Router, private http: HttpClient,
             private authService: AuthenticationService) {
             this.currentUser = this.authService.currentUserValue;
}

get f() { return this.editUserData.controls; }

ngOnInit() {
  this.uuid = this.dialogRef.id;
  this.getUserByID(this.uuid);


  this.userService.getUserByID(this.uuid).pipe(first()).subscribe(user => {
    this.selectedUser = user;


    this.editUserData.get('userName').setValue(user.userName);
    this.editUserData.get('email').setValue(user.email);
    this.editUserData.get('lastName').setValue(user.lastName);
    this.editUserData.get('firstName').setValue(user.firstName);
    this.editUserData.get('gender').setValue(user.gender);
    this.editUserData.get('profileColor').setValue(user.profileColor);


  });

  this.editUserData = this.formBuilder.group({
    firstName: ['', Validators.required],
    lastName: ['', Validators.required],
    userName: ['', Validators.required],
    email: ['', Validators.required],
    gender: [''],
    profileColor: ['']
  });
}
btnFunction(){
  this.btn = !this.btn;
}

getUserByID(uuid: string): void{
  this.userService.getUserByID(uuid)
  .subscribe(
    data => {
      this.currentUser = data;
    },
    error => {
      console.log(error);
    });
}

onSelectType(event){
  this.currentUser.gender = event.target.value;

}
editUser(): void {

  if(this.editUserData.invalid){
    return;
  }

  var updateUser = this.editUserData.value;


  if(this.selectedFile != null){
    updateUser.hasProfilePicture = true;
    updateUser.profilePicture = this.selectedFile.name;
  }else{
    updateUser.hasProfilePicture = this.selectedUser.hasProfilePicture;
    updateUser.profilePicture = this.selectedUser.profilePicture;
  }
  this.userService.editUser(this.currentUser.uuid, this.currentUser)
  .pipe(first())
  .subscribe(
    data => {
      if( this.selectedFile == null) {
      this.router.navigate(['/list-user']);
      }
    else {
      this.userService.upload(this.selectedUser.uuid, this.selectedFile)
      .subscribe(res => {
       console.log(res);
       window.location.reload();
      },    error => {
        console.log(error);
      });
      }
    },
    error => {
     console.log(error);
    });

    window.location.reload();
}


closeModal() {
  this.dialogRef.close();
}


deleteUser(): void{
this.userService.deleteUser(this.currentUser.uuid)
.subscribe(
response => {
  console.log(response);
  window.location.reload();
},
error => {
console.error();
});
}

onSelectFile(event) {

this.selectedFile =<File> event.target.files[0];

var reader = new FileReader();

reader.readAsDataURL(this.selectedFile); // read file as data url

reader.onload = (event) => { // called once readAsDataURL is completed
    this.url = event.target.result;
    console.log(this.selectedFile);

}
}
public delete(){
this.url = null;
}
}
