import { Component, OnInit } from '@angular/core';
import {UserService} from '../../service/user.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { Router } from '@angular/router';
import { User } from 'src/app/_models/user';
import { HttpClient } from '@angular/common/http';
import { DetailsUserComponent } from '../details-user/details-user.component';


@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css']
})
export class ListUserComponent implements OnInit {

  users = [];
  currentUser: User;
  imageToShow: any;
  isImageLoading: boolean;
  profilePictureUrl: any;
  user: User;

    constructor(private router: Router,
                private userService: UserService,
                public matDialog: MatDialog,
                private authService: AuthenticationService,
                private http: HttpClient){
    this.currentUser = this.authService.currentUserValue;
  }

   ngOnInit()
   {
    this.userService.getAllUsers().subscribe((data: any[]) => {
      console.log(data);
      this.users = data;
    });
  }
  openModal(uuid: string){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.id = uuid;
    dialogConfig.height = '660px';
    dialogConfig.width = '470px';

    const modalDialog = this.matDialog.open(DetailsUserComponent, dialogConfig);
  }

  logout(){
    this.authService.logout();
    this.router.navigate(['/login']);
  }


}



