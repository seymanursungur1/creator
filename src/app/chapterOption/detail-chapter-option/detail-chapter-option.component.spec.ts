import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailChapterOptionComponent } from './detail-chapter-option.component';

describe('DetailChapterOptionComponent', () => {
  let component: DetailChapterOptionComponent;
  let fixture: ComponentFixture<DetailChapterOptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailChapterOptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailChapterOptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
