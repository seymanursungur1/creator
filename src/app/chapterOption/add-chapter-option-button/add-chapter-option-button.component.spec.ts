import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddChapterOptionButtonComponent } from './add-chapter-option-button.component';

describe('AddChapterOptionButtonComponent', () => {
  let component: AddChapterOptionButtonComponent;
  let fixture: ComponentFixture<AddChapterOptionButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddChapterOptionButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddChapterOptionButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
