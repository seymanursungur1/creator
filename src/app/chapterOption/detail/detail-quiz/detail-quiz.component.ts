import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AnswerService } from 'src/app/service/answer.service';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { ChapterOptionService } from 'src/app/service/chapter-option.service';
import { ContentService } from 'src/app/service/content.service';
import { QuestionService } from 'src/app/service/question.service';
import { Answer } from 'src/app/_models/answer';
import { ButtonTeleport } from 'src/app/_models/button-teleport';
import { ChapterOption } from 'src/app/_models/chapter-option';
import { ButtonType } from 'src/app/_models/enums/button-type.enum';
import { ChapterOptionType } from 'src/app/_models/enums/chapter-option-type.enum';
import { TeleportTargetType } from 'src/app/_models/enums/teleport-target-type.enum';
import { VisibilityType } from 'src/app/_models/enums/visibility-type.enum';
import { Location } from 'src/app/_models/location';
import { Question } from 'src/app/_models/question';
import { User } from 'src/app/_models/user';


@Component({
  selector: 'app-detail-quiz',
  templateUrl: './detail-quiz.component.html',
  styleUrls: ['./detail-quiz.component.css']
})
export class DetailQuizComponent implements OnInit {

  showTextBoxContent = false;
  showTextBoxTeleport = false;
  showTextBoxChangeVars = false;
  showTextBoxSwitch = false;

  selectedQuestion: string;
  targetTypes = TeleportTargetType;
  chaptersExp = [];
  currentQuestion: Question;
  currentAnswer: Answer;
  answers = [];
  teleport: ButtonTeleport;
  buttonTypeTexts = ButtonType;
  buttonTypeText: any;
  currentUser : User;
  currentChapterOption: ChapterOption;
  visibilityTypes = VisibilityType;
  buttonTypes = ButtonType;
  optionTypes = ChapterOptionType;
  uuidQuestion: string;
  questions = [];
  contents = [];
  contentsQuestion = [];
  uuid: string;
  constructor(private chapterOptionService: ChapterOptionService,
              private route: ActivatedRoute,
              private router: Router,
              private authService: AuthenticationService,
              private contentService: ContentService,
              private questionService: QuestionService,
              private answerService: AnswerService) {
                this.currentChapterOption = new ChapterOption();
                this.currentChapterOption.Location = new Location();
                this.currentUser = this.authService.currentUserValue;
                this.currentChapterOption.Teleport = new ButtonTeleport();
                this.currentQuestion = new Question();
               }

  ngOnInit(): void {
    this.uuid = this.route.snapshot.paramMap.get('uuidChapterOption');
    console.log(this.uuid);

    this.getChapterOption(this.uuid);
    this.getContent(this.uuid);
    this.getQuestion(this.uuid);
    console.log(this.selectedQuestion);
  }
 logout(){
    this.authService.logout();
    this.router.navigate(['/login']);
}
detailContent(uuid): void{
  this.router.navigate(['detail-content/', uuid]);
}

deleteContent(uuid: string): void{
  this.contentService.deleteContent(uuid)
  .subscribe(
  response => {
    console.log(response);
    window.location.reload();
  },
  error => {
  console.error();
  });
  }

  deleteChapterOption(uuid: string): void{
    this.chapterOptionService.deleteChapterOption(uuid)
    .subscribe(
    response => {
      console.log(response);
      window.location.reload();
    },
    error => {
    console.error();
    });
  }
  onSelectedQuestion(event){
    this.selectedQuestion = event.target.value;
    console.log(this.selectedQuestion);

    this.answerService.getAnswerByQuestionId(this.selectedQuestion)
    .subscribe(
      data => {
        this.answers = data;
        console.log(this.answers);

      },
      error => {
        console.log(error);
      }
    );
  }

  getChapterOption(uuid: string): void{
    this.chapterOptionService.getChapterOptionByID(uuid)
    .subscribe(
      data => {
        this.currentChapterOption = data;
        console.log(this.currentChapterOption);

        if (this.currentChapterOption.ButtonType == this.buttonTypes.Content){
    this.showTextBoxChangeVars =  false;
    this.showTextBoxSwitch = false;
    this.showTextBoxTeleport = false;
    this.showTextBoxContent = !this.showTextBoxContent;
  }
        if (this.currentChapterOption.ButtonType == this.buttonTypes.Teleport && this.currentChapterOption.OptionType == this.optionTypes.Button){
    this.showTextBoxChangeVars =  false;
    this.showTextBoxContent = false;
    this.showTextBoxSwitch = false;
    this.showTextBoxTeleport = !this.showTextBoxTeleport;
  }
        if(this.currentChapterOption.ButtonType == this.buttonTypes.ChangeVars){
    this.showTextBoxContent = false;
    this.showTextBoxSwitch = false;
    this.showTextBoxTeleport = false;
    this.showTextBoxChangeVars = !this.showTextBoxChangeVars;
  }
        if (this.currentChapterOption.ButtonType == this.buttonTypes.Switch){
    this.showTextBoxChangeVars =  false;
    this.showTextBoxContent = false;
    this.showTextBoxTeleport = false;
    this.showTextBoxSwitch = !this.showTextBoxSwitch;
  }
      },
      error => {
        console.log(error);
      }
    );
  }



  getContent(uuid: string): void{
    this.contentService.getContentByItemID(uuid)
    .subscribe(
      data => {
        this.contents = data;
      },
      error => {
        console.log(error);
      }
    );
  }
  getQuestion(uuid: string): void{
    this.questionService.getQuestionByQuizId(uuid)
    .subscribe(
      data => {
        this.questions = data;
        console.log(this.questions);

      },
      error =>{
        console.log(error);
      }
    );
  }

  /*getAnswer(uuid: string): void{
    this.answerService.getAnswerByQuestionId()



    this.contentService.getContentByItemID(this.selectedQuestion)
      .subscribe(
        data => {
          this.contentsQuestion = data;
          console.log(this.contentsQuestion);
        },
        error => {
          console.log(error);
        }
      );

  }*/
  onSelectTargetType(event){
    this.teleport.targetType =  event.target.value;
    console.log(this.teleport.targetType);
  }

  onSelectTeleportTo(event){
    this.teleport.teleportTarget =  event.target.value;

  }

  editChapterOption(): void {
    console.log(JSON.stringify(this.currentChapterOption));
    this.chapterOptionService.editChapterOption(this.uuid, this.currentChapterOption)
    .pipe(first())
    .subscribe(res => {
         console.log(res);
         window.location.reload();
        },    error => {
          console.log(error);
        });
  }

  onSelectVisibility(event){
    this.currentChapterOption.Visibility = event.target.value;
  }


  onSelectButtonType(event){
    this.currentChapterOption.ButtonType = event.target.value;
    this.buttonTypeText = this.currentChapterOption.ButtonType;
    console.log(this.buttonTypeText);
    this.showTextBoxChangeVars =  false;
    this.showTextBoxContent = false;
    this.showTextBoxSwitch = false;
    this.showTextBoxTeleport = false;

    if (this.buttonTypeText == '0'){
    this.showTextBoxChangeVars =  false;
    this.showTextBoxSwitch = false;
    this.showTextBoxTeleport = false;
    this.showTextBoxContent = !this.showTextBoxContent;
  }
    if (this.buttonTypeText == '1'){
    this.showTextBoxChangeVars =  false;
    this.showTextBoxContent = false;
    this.showTextBoxSwitch = false;
    this.showTextBoxTeleport = !this.showTextBoxTeleport;
  }
    if(this.buttonTypeText == '2'){
    this.showTextBoxContent = false;
    this.showTextBoxSwitch = false;
    this.showTextBoxTeleport = false;
    this.showTextBoxChangeVars = !this.showTextBoxChangeVars;
  }
    if (this.buttonTypeText == '3'){
    this.showTextBoxChangeVars =  false;
    this.showTextBoxContent = false;
    this.showTextBoxTeleport = false;
    this.showTextBoxSwitch = !this.showTextBoxSwitch;
  }
    console.log(this.showTextBoxContent);
    console.log(this.showTextBoxTeleport);
    console.log(this.showTextBoxChangeVars);
    console.log(this.showTextBoxSwitch);
    /*if(this.buttonTypeText = 0){
      this.showTextBoxContent = true;
      this.showTextBoxTeleport = false;
    }
    if(this.buttonTypeText = 1){
      this.showTextBoxTeleport = true;
    }*/
    }


}
