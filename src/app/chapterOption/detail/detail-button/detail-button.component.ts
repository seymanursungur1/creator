import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { stringList } from 'aws-sdk/clients/datapipeline';
import { first } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { ChapterOptionService } from 'src/app/service/chapter-option.service';
import { ContentService } from 'src/app/service/content.service';
import { ExperienceService } from 'src/app/service/experience.service';
import { ButtonTeleport } from 'src/app/_models/button-teleport';
import { ChapterOption } from 'src/app/_models/chapter-option';
import { ButtonType } from 'src/app/_models/enums/button-type.enum';
import { ChapterOptionType } from 'src/app/_models/enums/chapter-option-type.enum';
import { TeleportTargetType } from 'src/app/_models/enums/teleport-target-type.enum';
import { VisibilityType } from 'src/app/_models/enums/visibility-type.enum';
import { Experience } from 'src/app/_models/experience';
import { Location } from 'src/app/_models/location';
import { User } from 'src/app/_models/user';


@Component({
  selector: 'app-detail-button',
  templateUrl: './detail-button.component.html',
  styleUrls: ['./detail-button.component.css']
})
export class DetailButtonComponent implements OnInit {

  showTextBoxContent = false;
  showTextBoxTeleport = false;
  showTextBoxChangeVars = false;
  showTextBoxSwitch = false;

  targetTypeText: string;
  teleportToText: string;
  uuidCurrentExperience: string;
  currentExperience: Experience;
  targetTypes = TeleportTargetType;
  chaptersExp = [];
  teleport: ButtonTeleport;
  buttonTypeTexts = ButtonType;
  buttonTypeText: any;
  currentUser : User;
  currentChapterOption: ChapterOption;
  visibilityTypes = VisibilityType;
  buttonTypes = ButtonType;
  optionTypes = ChapterOptionType;

  contents = [];
  uuidChapterOption: string;
  constructor(private chapterOptionService: ChapterOptionService,
              private route: ActivatedRoute,
              private router: Router,
              private experienceService: ExperienceService,
              private authService: AuthenticationService,
              private contentService: ContentService) {
                this.currentChapterOption = new ChapterOption();
                this.currentChapterOption.Location = new Location();
                this.currentUser = this.authService.currentUserValue;
                this.currentChapterOption.Teleport = new ButtonTeleport();
               }

  ngOnInit(): void {
    this.uuidChapterOption = this.route.snapshot.paramMap.get('uuidChapterOption');
    this.uuidCurrentExperience = this.route.snapshot.paramMap.get('uuidExperience');
    console.log(this.uuidChapterOption);

    this.getChapterOption(this.uuidChapterOption);
    this.getContent(this.uuidChapterOption);
    this.getExperience(this.uuidCurrentExperience);

  }
 logout(){
    this.authService.logout();
    this.router.navigate(['/login']);
}
detailContent(uuid): void{
  this.router.navigate(['detail-content/', uuid]);
}
deleteChapterOption(uuid: string): void{
  this.chapterOptionService.deleteChapterOption(uuid)
  .subscribe(
  response => {
    console.log(response);
    window.location.reload();
  },
  error => {
  console.error();
  });
}
getExperience(uuid: string): void{
  this.experienceService.getExperienceByID(uuid)
  .subscribe(
    data => {
      this.currentExperience = data;
      this.chaptersExp = this.currentExperience.chapters;
      console.log(this.chaptersExp);
    },
    error => {
      console.log(error);
    }
  );
}
deleteContent(uuid: string): void{
  this.contentService.deleteContent(uuid)
  .subscribe(
  response => {
    console.log(response);
    window.location.reload();
  },
  error => {
  console.error();
  });
  }
  getChapterOption(uuid: string): void{
    this.chapterOptionService.getChapterOptionByID(uuid)
    .subscribe(
      data => {
        this.currentChapterOption = data;
        console.log(this.currentChapterOption);

        if (this.currentChapterOption.ButtonType == this.buttonTypes.Content){
    this.showTextBoxChangeVars =  false;
    this.showTextBoxSwitch = false;
    this.showTextBoxTeleport = false;
    this.showTextBoxContent = !this.showTextBoxContent;
  }
        if (this.currentChapterOption.ButtonType == this.buttonTypes.Teleport && this.currentChapterOption.OptionType == this.optionTypes.Button){
    this.showTextBoxChangeVars =  false;
    this.showTextBoxContent = false;
    this.showTextBoxSwitch = false;
    this.showTextBoxTeleport = !this.showTextBoxTeleport;
  }
        if(this.currentChapterOption.ButtonType == this.buttonTypes.ChangeVars){
    this.showTextBoxContent = false;
    this.showTextBoxSwitch = false;
    this.showTextBoxTeleport = false;
    this.showTextBoxChangeVars = !this.showTextBoxChangeVars;
  }
        if (this.currentChapterOption.ButtonType == this.buttonTypes.Switch){
    this.showTextBoxChangeVars =  false;
    this.showTextBoxContent = false;
    this.showTextBoxTeleport = false;
    this.showTextBoxSwitch = !this.showTextBoxSwitch;
  }
      },
      error => {
        console.log(error);
      }
    );
  }


  textBoxActive(){

}
  getContent(uuid: string): void{
    this.contentService.getContentByItemID(uuid)
    .subscribe(
      data => {
        this.contents = data;
       // console.log("contents:")
       // console.log(data);
      },
      error => {
        console.log(error);
      }
    );
  }

  onSelectTargetType(event){
   // this.teleport.targetType =  event.target.value;
    //console.log(this.teleport.targetType);

    this.targetTypeText = event.target.value;
    console.log(this.targetTypeText);

  }

  onSelectTeleportTo(event){
    //this.teleport.teleportTarget =  event.target.value;
      //this.teleportA.targetType = event.target.value;
     // console.log(this.teleport.teleportTarget);
      this.teleportToText =  event.target.value;
      console.log(this.teleportToText);
      this.teleport.teleportTarget = this.teleportToText;
      console.log(this.teleport.teleportTarget);
  }

  editChapterOption(): void {
    console.log(JSON.stringify(this.currentChapterOption));
    this.chapterOptionService.editChapterOption(this.uuidChapterOption, this.currentChapterOption)
    .pipe(first())
    .subscribe(res => {
         console.log(res);
         window.location.reload();
        },    error => {
          console.log(error);
        });
  }

  onSelectVisibility(event){
    this.currentChapterOption.Visibility = event.target.value;
  }


  onSelectButtonType(event){
    this.currentChapterOption.ButtonType = event.target.value;
    this.buttonTypeText = this.currentChapterOption.ButtonType;
    console.log(this.buttonTypeText);
    this.showTextBoxChangeVars =  false;
    this.showTextBoxContent = false;
    this.showTextBoxSwitch = false;
    this.showTextBoxTeleport = false;

    if (this.buttonTypeText == '0'){
    this.showTextBoxChangeVars =  false;
    this.showTextBoxSwitch = false;
    this.showTextBoxTeleport = false;
    this.showTextBoxContent = !this.showTextBoxContent;
  }
    if (this.buttonTypeText == '1'){
    this.showTextBoxChangeVars =  false;
    this.showTextBoxContent = false;
    this.showTextBoxSwitch = false;
    this.showTextBoxTeleport = !this.showTextBoxTeleport;
  }
    if(this.buttonTypeText == '2'){
    this.showTextBoxContent = false;
    this.showTextBoxSwitch = false;
    this.showTextBoxTeleport = false;
    this.showTextBoxChangeVars = !this.showTextBoxChangeVars;
  }
    if (this.buttonTypeText == '3'){
    this.showTextBoxChangeVars =  false;
    this.showTextBoxContent = false;
    this.showTextBoxTeleport = false;
    this.showTextBoxSwitch = !this.showTextBoxSwitch;
  }
    console.log(this.showTextBoxContent);
    console.log(this.showTextBoxTeleport);
    console.log(this.showTextBoxChangeVars);
    console.log(this.showTextBoxSwitch);
    /*if(this.buttonTypeText = 0){
      this.showTextBoxContent = true;
      this.showTextBoxTeleport = false;
    }
    if(this.buttonTypeText = 1){
      this.showTextBoxTeleport = true;
    }*/
    }


}
