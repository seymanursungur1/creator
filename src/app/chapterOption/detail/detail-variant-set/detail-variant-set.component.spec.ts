import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailVariantSetComponent } from './detail-variant-set.component';

describe('DetailVariantSetComponent', () => {
  let component: DetailVariantSetComponent;
  let fixture: ComponentFixture<DetailVariantSetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailVariantSetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailVariantSetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
