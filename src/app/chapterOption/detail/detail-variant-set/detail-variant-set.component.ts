import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { ChapterOptionService } from 'src/app/service/chapter-option.service';
import { ContentService } from 'src/app/service/content.service';
import { ButtonTeleport } from 'src/app/_models/button-teleport';
import { ChapterOption } from 'src/app/_models/chapter-option';
import { ButtonType } from 'src/app/_models/enums/button-type.enum';
import { ChapterOptionType } from 'src/app/_models/enums/chapter-option-type.enum';
import { TeleportTargetType } from 'src/app/_models/enums/teleport-target-type.enum';
import { VisibilityType } from 'src/app/_models/enums/visibility-type.enum';
import { Location } from 'src/app/_models/location';
import { User } from 'src/app/_models/user';
import { Variant } from 'src/app/_models/variant';
import { VariantSet } from 'src/app/_models/variantSet';

@Component({
  selector: 'app-detail-variant-set',
  templateUrl: './detail-variant-set.component.html',
  styleUrls: ['./detail-variant-set.component.css']
})
export class DetailVariantSetComponent implements OnInit {


  variant = {
    alias : '',
    variantName: ''
  };
  canAddVariant = false;
  currentUser: User;
  variants = [];
  currentChapterOption: VariantSet;
  chapterOption = [];
  visibilityTypes = VisibilityType;
  optionTypes = ChapterOptionType;
  contents = [];
  uuid: string;
  constructor(private chapterOptionService: ChapterOptionService,
              private route: ActivatedRoute,
              private router: Router,
              private authService: AuthenticationService,
              private contentService: ContentService) {
                this.currentChapterOption = new VariantSet();
                this.currentChapterOption.Location = new Location();
                this.currentUser = this.authService.currentUserValue;
                this.currentChapterOption.Variants = new Array<Variant>();
               }

  ngOnInit(): void {
    this.uuid = this.route.snapshot.paramMap.get('uuidChapterOption');
    console.log(this.uuid);

    this.getChapterOption(this.uuid);

    this.getContent(this.uuid);

  }

  deleteChapterOption(uuid: string): void{
    this.chapterOptionService.deleteChapterOption(uuid)
    .subscribe(
    response => {
      console.log(response);
      window.location.reload();
    },
    error => {
    console.error();
    });
  }
 logout(){
    this.authService.logout();
    this.router.navigate(['/login']);
}
detailContent(uuid): void{
  this.router.navigate(['detail-content/', uuid]);
}

deleteContent(uuid: string): void{
  this.contentService.deleteContent(uuid)
  .subscribe(
  response => {
    console.log(response);
    window.location.reload();
  },
  error => {
  console.error();
  });
  }
  getChapterOption(uuid: string): void{
    this.chapterOptionService.getChapterOptionByID(uuid)
    .subscribe(
      data => {
        this.currentChapterOption = data;
        console.log(this.currentChapterOption);
        this.variants = this.currentChapterOption.Variants;
        console.log(this.variants);
      },
      error => {
        console.log(error);
      }
    );
  }

  addVariant(){
    this.canAddVariant = !this.canAddVariant;
  }
  getContent(uuid: string): void{
    this.contentService.getContentByItemID(uuid)
    .subscribe(
      data => {
        this.contents = data;
      },
      error => {
        console.log(error);
      }
    );
  }

  editChapterOption(): void {

    const variantList = new Variant();
    variantList.alias = this.variant.alias;
    variantList.variantName = this.variant.variantName;
    this.currentChapterOption.Variants.push(variantList);

    console.log(JSON.stringify(this.currentChapterOption));
    this.chapterOptionService.editChapterOption(this.uuid, this.currentChapterOption)
    .pipe(first())
    .subscribe(res => {
         console.log(res);
         window.location.reload();
        },    error => {
          console.log(error);
        });
  }

  onSelectVisibility(event){
    this.currentChapterOption.Visibility = event.target.value;
  }




}
