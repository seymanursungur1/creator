import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AnswerService } from 'src/app/service/answer.service';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { ChapterOptionService } from 'src/app/service/chapter-option.service';
import { ChapterService } from 'src/app/service/chapter.service';
import { ContentService } from 'src/app/service/content.service';
import { ExperienceService } from 'src/app/service/experience.service';
import { QuestionService } from 'src/app/service/question.service';
import { Answer } from 'src/app/_models/answer';
import { Chapter } from 'src/app/_models/chapter';
import { Content } from 'src/app/_models/content';
import { ChapterOptionType } from 'src/app/_models/enums/chapter-option-type.enum';
import { ContentType } from 'src/app/_models/enums/content-type.enum';
import { VisibilityType } from 'src/app/_models/enums/visibility-type.enum';
import { Experience } from 'src/app/_models/experience';
import { Location } from 'src/app/_models/location';
import { Question } from 'src/app/_models/question';
import { Quiz } from 'src/app/_models/quiz';
import { Rotation } from 'src/app/_models/rotation';
import { User } from 'src/app/_models/user';
import { Variant } from 'src/app/_models/variant';
import { VariantSet } from 'src/app/_models/variantSet';

@Component({
  selector: 'app-add-quiz',
  templateUrl: './add-quiz.component.html',
  styleUrls: ['./add-quiz.component.css']
})
export class AddQuizComponent implements OnInit {


  currentChapter: Chapter;
  currentQuiz: Quiz;
  currentAnswer: Answer;
  currentQuestion: Question;
  chaptersExp = [];
  quizes = [];
  variantSets = [];
  comments = [];
  questions = [];
  answers = [];
  selectedChapterOptionUuid: string;
  x:string;
  selectedQuizUuid: string;
  selectedQuestionUuid: string;
  selectedAnswerUuid: string;

  currentUser: User;
  targetTypeText: string;
  selectedFile: File = null;
  url: any;
  typeContent: string; // returns selected type as string
  visibilityContent: string;
  visibilityQuiz: string;
  languageContent: string;
  qualityContent: string;
  qualities = [];
  uuidCurrentExperience: string;
  uuidCurrentChapter: string;

  quizClass: Quiz;
   chapterArray = new Chapter();
  chapter = {
      alias: '',
      maxOpendHotspots : 0,
      buttons: [],
      chapterOptions: [],
      comments: [],
      content: [],
      quizes: [],
      navigateOnCompletion : false,
      navigationMenuEnabled : false,
      order : 0,
      pointerAllowed : false,
  };
  currentExperience: Experience;
  chapters = [];
  contents = [];
  contentTypes = ContentType;
  visibilityTypes = VisibilityType;
  chapterOptionsList = [];
  experience = {
      chapters: []
  };
  location = {
    alias: '',
    x: null,
    y: null,
    z: null
  };

  rotation = {
    alias: '',
    x: 0,
    y: 0,
    z: 0
  };
  content = {
    key: '',
    version: 0,
    alias: '',
    type:  ContentType [''],
    value: '',
    quality: '',
    language: '',
    visibility: VisibilityType['']
  };

  contentQ = {
    key: '',
    version: 0,
    alias: '',
    type:  ContentType [''],
    value: '',
    quality: '',
    language: '',
    visibility: VisibilityType['']
  };

  contentA = {
    key: '',
    version: 0,
    alias: '',
    type:  ContentType [''],
    value: '',
    quality: '',
    language: '',
    visibility: VisibilityType['']
  };



  optionTypeValue : any;
  quiz = {
      uuid: '',
      autoOpen: false,
      optionType: ChapterOptionType.Quiz,
      questions: [],
      order: null,
      visibility: VisibilityType[''],
      location: this.location,
      rotation: this.rotation,
      content: [],
      alias: ''
  };


  question = {
      uuid: '',
      order: 0,
      alias: 'Question',
      content: [],
      answers: []
  };

  answer = {
    uuid: '',
    order: 0,
    alias: 'Answer',
    content: [],
    correct: false
  };

  locations: any[] = [];
  rotations: any[] = [];

  regTypeSelectedOption: string="";

  constructor( private authService: AuthenticationService,
               private router: Router,
               private experienceService: ExperienceService,
               private route: ActivatedRoute,
               private chapterService: ChapterService,
               private chapterOptionService: ChapterOptionService,
               private contentService: ContentService,
               private questionService: QuestionService,
               private answerService: AnswerService) {
                this.currentUser = this.authService.currentUserValue;
               }

  ngOnInit(): void {
    this.uuidCurrentExperience = this.route.snapshot.paramMap.get('uuidExperience');
    this.uuidCurrentChapter = this.route.snapshot.paramMap.get('uuidChapter');
    console.log(this.uuidCurrentExperience);
    console.log(this.uuidCurrentChapter);
    this.getExperience(this.uuidCurrentExperience);
    this.getChapterOptions(this.uuidCurrentChapter);


  }


  getChapterOptions(uuid: string): void{
    this.chapterOptionService.getChapterOptionByChapterID(uuid)
    .subscribe(
      data => {
        this.chapterOptionsList = data;
        console.log("chapterOptions:");
        console.log(this.chapterOptionsList);
      },
      error => {
        console.log(error);
      }
    );
  }

  getExperience(uuid: string): void{
    this.experienceService.getExperienceByID(uuid)
    .subscribe(
      data => {
        this.currentExperience = data;
        this.chaptersExp = this.currentExperience.chapters;
        const result = this.chaptersExp.find(({uuid}) => uuid === this.uuidCurrentChapter);
        console.log(result);
        this.quizes = result.quizes;
        //console.log(this.quizes);
        this.variantSets = result.variantSets;
        console.log(this.variantSets);
      },
      error => {
        console.log(error);
      }
    );
  }
  getChapter(uuid: string): void{
    this.chapterService.getChapterById(uuid)
    .subscribe(
      data => {
        this.currentChapter = data;
      },
      error => {
        console.log(error);
      }
    );
  }




  addQuiz(): void{
    const addQuizData = {
      autoOpen: this.quiz.autoOpen,
      optionType: this.quiz.optionType,
      questions: this.quiz.questions,
      order: this.quiz.order,
      visibility: this.quiz.visibility,
      location: this.quiz.location,
      rotation: this.quiz.rotation,
      content: this.quiz.content,
      alias: this.quiz.alias
    };

    const addQuestionData = {
      order: this.question.order,
      alias: this.question.alias,
      content: this.question.content,
      answers: this.question.answers
    };

    const addAnswerData = {
      order: this.answer.order,
      alias: this.answer.alias,
      content: this.answer.content,
      correct: this.answer.correct

    };
    const addExperienceData = {
      chapters: this.experience.chapters,
    };

    const locationObject = new Location();
    locationObject.x = this.location.x;
    locationObject.y = this.location.y;
    locationObject.z = this.location.z;
    locationObject.alias = this.location.alias;
    this.locations.push(locationObject);

    const rotationObject = new Rotation();
    rotationObject.x = this.rotation.x;
    rotationObject.y = this.rotation.y;
    rotationObject.z = this.rotation.z;
    rotationObject.alias = this.rotation.alias;
    this.locations.push(rotationObject);

    addQuestionData.answers = new Array<Question>();
    addQuestionData.answers.push(addAnswerData);

    addQuizData.questions = new Array<Quiz>();
    addQuizData.questions.push(addQuestionData);

    /*addQuestionData.content = new Array<Question>();

    const contentQuestion = new Content();
    contentQuestion.key = "Question";
    contentQuestion.value = this.content.value;
    addQuestionData.content.push(contentQuestion);*/
    const result = this.currentExperience.chapters.find(({uuid}) => uuid === this.uuidCurrentChapter);
    console.log(result);
    this.quizes = result.chapterOptions;

    this.quizes.push(addQuizData);


    this.currentChapter = result;
    console.log(addExperienceData.chapters);
    addExperienceData.chapters.push(this.currentChapter);
    console.log(addExperienceData.chapters);

    this.currentExperience.chapters = addExperienceData.chapters;


    this.experienceService.editExperience(this.currentExperience.uuid, this.currentExperience)
    .subscribe(res => {
      console.log(JSON.stringify(this.currentExperience));
      console.log(res);

    },
      error => {
        console.log(error);
      }
    );
  }


onSelectVisibilityQuiz(event){
  this.quiz.visibility = event.target.value;
  this.visibilityQuiz = this.quiz.visibility;
}


onSelectQuality(event){
  this.content.quality = event.target.value;
  this.contents.filter(element => {
    if (element.quality == event.target.value){
      this.qualityContent = element.quality;
    }
  });
}

onSelectType(event){
  this.content.type = event.target.value;
  this.contents.filter(element => {
    if (element.type == event.target.value){
      this.typeContent = element.type;
    }
  });

}
onSelected(event) {
  this.selectedFile = (event.target.files[0] as File);
  const reader = new FileReader();
  reader.readAsDataURL(this.selectedFile);
  reader.onload = (_event) => {
    this.url = reader.result;
  };
  console.log(this.selectedFile);
  console.log(this.url);
}

  logout(){
    this.authService.logout();
    this.router.navigate(['/login']);
}

onSelectedChapterOption(event){
  this.selectedChapterOptionUuid = event.target.value;
  console.log(this.selectedChapterOptionUuid);
  this.selectedQuizUuid  = this.selectedChapterOptionUuid;
  console.log(this.selectedQuizUuid);
}

onSelectedQuestion(event){
  this.selectedQuestionUuid = event.target.value;
  console.log(this.selectedQuestionUuid);
}
createContent(): void{

  const addContentData = {
  key: this.content.key,
  version: this.content.version,
  alias: this.content.alias,
  value: this.content.value,
  type: this.content.type,
  quality: this.content.quality,
  language: this.content.language,
  visibility: this.content.visibility
  };

  const result = this.chapterOptionsList.find(({uuid}) => uuid === this.selectedChapterOptionUuid);
  console.log(result);

  console.log(result.optionType);
  this.optionTypeValue = result.optionType;
  if(this.optionTypeValue == "0"){
    addContentData.key = "Quiz";
    this.selectedQuizUuid = this.selectedChapterOptionUuid;
    console.log(this.selectedQuizUuid);
  }



  addContentData.quality = this.qualityContent;
  addContentData.language = this.languageContent;

 /* var contentClass = new Content();
  contentClass.uuid  = this.uuidContent;

  if(this.selectedFile == null ){
    addContentData.value = this.content.value;
   // addContentData.value = null;
  }
  else {
    addContentData.value = this.selectedFile.name;
  }*/

 /* this.contentService.addContentByItemId(this.selectedChapterOptionUuid, addContentData)

    .subscribe(data => {
      var cD = data as Content;
      console.log(data);
      console.log(this.selectedFile);
      if(this.typeContent == "0" || this.selectedFile == null){ // als text gekozen is en geen foto is, value is value
        console.log('you did it');
         // addContentData.value = this.content.value;
      }
      else{
        data.value = this.selectedFile.name;
        console.log("dedeefe");
        console.log(this.selectedFile);
        this.contentService.upload( data.uuid, this.selectedChapterOptionUuid, this.selectedFile)
      .subscribe(res => {
        console.log(res);

        this.router.navigate(['/list-experience']);

    });
      }
    },
      error => {
        console.log(error);
    }
);*/

  console.log(addContentData);
}


getQuestion(){
  this.questionService.getQuestionByQuizId(this.selectedQuizUuid)
  .subscribe(
    data => {
      this.currentQuestion = data;
      this.x = this.currentQuestion[0].uuid;
      this.selectedQuestionUuid = this.x;
      console.log(this.currentQuestion);
      console.log(data);
      console.log(this.selectedQuestionUuid);

      this.contentService.addContentByItemId(this.selectedQuestionUuid, addContentData)

      .subscribe(data => {
        var cD = data as Content;
        console.log(data);
       // console.log(this.selectedFile);
        if(this.typeContent == "0" || this.selectedFile == null){ // als text gekozen is en geen foto is, value is value
          console.log('you did it');
           // addContentData.value = this.content.value;

        }
        else{
          data.value = this.selectedFile.name;
          console.log("dedeefe");
          console.log(this.selectedFile);
          this.contentService.upload( data.uuid, this.selectedChapterOptionUuid, this.selectedFile)
        .subscribe(res => {
          console.log(res);

          this.router.navigate(['/list-experience']);

      });
        }
      },
        error => {
          console.log(error);
      }
  );
    },
    error => {
      console.log(error);
    }
  );

  const addContentData = {
    key: this.contentQ.key,
    version: this.contentQ.version,
    alias: this.contentQ.alias,
    value: this.contentQ.value,
    type: this.contentQ.type,
    quality: this.contentQ.quality,
    language: this.contentQ.language,
    visibility: this.contentQ.visibility
    };


  addContentData.key = "Question";
  addContentData.type = "0";
  addContentData.version = 0;
  addContentData.value = "leeg";



  addContentData.quality = this.qualityContent;
  addContentData.language = this.languageContent;

  console.log(this.selectedQuestionUuid);

  console.log(JSON.stringify(addContentData));
  console.log(addContentData);
}


getAnswer(): void{
  this.answerService.getAnswerByQuestionId(this.selectedQuestionUuid)
  .subscribe(
    data => {
      this.currentAnswer = data;
      this.selectedAnswerUuid = this.currentAnswer[0].uuid;
      console.log(this.currentAnswer);
      console.log(this.selectedAnswerUuid);
      this.contentService.addContentByItemId(this.selectedAnswerUuid, addContentData)

      .subscribe(data => {
        var cD = data as Content;
        console.log(data);
       // console.log(this.selectedFile);
        if(this.typeContent == "0" || this.selectedFile == null){ // als text gekozen is en geen foto is, value is value
          console.log('you did it');
           // addContentData.value = this.content.value;

        }
        else{
          data.value = this.selectedFile.name;
          console.log("dedeefe");
          console.log(this.selectedFile);
          this.contentService.upload( data.uuid, this.selectedChapterOptionUuid, this.selectedFile)
        .subscribe(res => {
          console.log(res);

          this.router.navigate(['/list-experience']);

      });
        }
      },
        error => {
          console.log(error);
      }
  );
    },
    error => {
      console.log(error);
    }
  );

  const addContentData = {
    key: this.contentA.key,
    version: this.contentA.version,
    alias: this.contentA.alias,
    value: this.contentA.value,
    type: this.contentA.type,
    quality: this.contentA.quality,
    language: this.contentA.language,
    visibility: this.contentA.visibility
    };


  addContentData.key = "Answer";
  addContentData.type = "0";
  addContentData.version = 0;
  addContentData.value = "leeg";



  addContentData.quality = this.qualityContent;
  addContentData.language = this.languageContent;


  console.log(this.selectedQuestionUuid);

  console.log(JSON.stringify(addContentData));
  console.log(addContentData);
}

  addContentQuestion(){
    const addContentData = {
      key: this.contentQ.key,
      version: this.contentQ.version,
      alias: this.contentQ.alias,
      value: this.contentQ.value,
      type: this.contentQ.type,
      quality: this.contentQ.quality,
      language: this.contentQ.language,
      visibility: this.contentQ.visibility
      };


    addContentData.key = "Question";
    addContentData.type = "0";


    addContentData.quality = this.qualityContent;
    addContentData.language = this.languageContent;


    console.log(this.selectedQuestionUuid);
    this.contentService.addContentByItemId(this.selectedQuestionUuid, addContentData)

        .subscribe(data => {
          var cD = data as Content;
          console.log(data);
          console.log(this.selectedFile);
          if(this.typeContent == "0" || this.selectedFile == null){ // als text gekozen is en geen foto is, value is value
            console.log('you did it');
             // addContentData.value = this.content.value;

          }
          else{
            data.value = this.selectedFile.name;
            console.log("dedeefe");
            console.log(this.selectedFile);
            this.contentService.upload( data.uuid, this.selectedChapterOptionUuid, this.selectedFile)
          .subscribe(res => {
            console.log(res);

            this.router.navigate(['/list-experience']);

        });
          }
        },
          error => {
            console.log(error);
        }
    );
    console.log(JSON.stringify(addContentData));
    console.log(addContentData);
  }



}
