import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddVariantSetComponent } from './add-variant-set.component';

describe('AddVariantSetComponent', () => {
  let component: AddVariantSetComponent;
  let fixture: ComponentFixture<AddVariantSetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddVariantSetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddVariantSetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
