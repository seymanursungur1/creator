import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AnswerService } from 'src/app/service/answer.service';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { ChapterOptionService } from 'src/app/service/chapter-option.service';
import { ChapterService } from 'src/app/service/chapter.service';
import { ContentService } from 'src/app/service/content.service';
import { ExperienceService } from 'src/app/service/experience.service';
import { QuestionService } from 'src/app/service/question.service';
import { Answer } from 'src/app/_models/answer';
import { Button } from 'src/app/_models/button';
import { ButtonTeleport } from 'src/app/_models/button-teleport';
import { Chapter } from 'src/app/_models/chapter';
import { Content } from 'src/app/_models/content';
import { ButtonType } from 'src/app/_models/enums/button-type.enum';
import { ChapterOptionType } from 'src/app/_models/enums/chapter-option-type.enum';
import { ContentType } from 'src/app/_models/enums/content-type.enum';
import { TeleportTargetType } from 'src/app/_models/enums/teleport-target-type.enum';
import { VisibilityType } from 'src/app/_models/enums/visibility-type.enum';
import { Experience } from 'src/app/_models/experience';
import { Location } from 'src/app/_models/location';
import { Question } from 'src/app/_models/question';
import { Quiz } from 'src/app/_models/quiz';
import { Rotation } from 'src/app/_models/rotation';
import { User } from 'src/app/_models/user';

@Component({
  selector: 'app-add-button',
  templateUrl: './add-button.component.html',
  styleUrls: ['./add-button.component.css']
})
export class AddButtonComponent implements OnInit {
  currentChapter: Chapter;
  showTextBoxContent = false;
  showTextBoxTeleport = false;
  showTextBoxChangeVars = false;
  showTextBoxSwitch = false;

  chaptersExp = [];
  buttons = [];
  selectedChapterOptionUuid: string;
  currentUser: User;
  buttonTypeTexts = ButtonType;
  buttonTypeText: string;
  teleportToText: string;
  selectedFile: File = null;
  url: any;
  typeContent: string; // returns selected type as string
  visibilityContent: string;
  visibilityButton: string;
  languageContent: string;
  qualityContent: string;
  qualities = [];
  uuidCurrentExperience: string;
  uuidCurrentChapter: string;
  targetTypes = TeleportTargetType;
  targetTypeText: string;
   chapterArray = new Chapter();
  chapter = {
      alias: '',
      maxOpendHotspots : 0,
      buttons: [],
      chapterOptions: [],
      comments: [],
      content: [],
      quizes: [],
      navigateOnCompletion : false,
      navigationMenuEnabled : false,
      order : 0,
      pointerAllowed : false,
  };
  currentExperience: Experience;
  chapters = [];
  contents = [];
  contentTypes = ContentType;
  visibilityTypes = VisibilityType;
  chapterOptionsList = [];
  experience = {
      chapters: []
  };
  location = {
    alias: '',
    x: null,
    y: null,
    z: null
  };

  rotation = {
    alias: '',
    x: 0,
    y: 0,
    z: 0
  };
  content = {
    key: '',
    version: 0,
    alias: '',
    type:  ContentType [''],
    value: '',
    quality: '',
    language: '',
    visibility: VisibilityType['']
  };

  teleport = {
    teleportTarget: '',
    targetType: TeleportTargetType['']
  };

  optionTypeValue : any;
  button = {
    alias: '',
    autoOpen: false,
    buttonType: ButtonType[''],
    content: [],
    location: this.location,
    order: null,
    optionType: ChapterOptionType.Button,
    rotation: this.rotation,
    teleport: this.teleport,
    visibility: VisibilityType['']
  };

  locations: any[] = [];
  rotations: any[] = [];
  teleports: any[] = [];


  constructor( private authService: AuthenticationService,
               private router: Router,
               private experienceService: ExperienceService,
               private route: ActivatedRoute,
               private chapterService: ChapterService,
               private chapterOptionService: ChapterOptionService,
               private contentService: ContentService,
               private questionService: QuestionService,
               private answerService: AnswerService) {
                this.currentUser = this.authService.currentUserValue;
               }

  ngOnInit(): void {
   this.uuidCurrentExperience = this.route.snapshot.paramMap.get('uuidExperience');
   console.log(this.uuidCurrentExperience);
   this.uuidCurrentChapter = this.route.snapshot.paramMap.get('uuidChapter');
   console.log(this.uuidCurrentExperience);
   console.log(this.uuidCurrentChapter);
   this.getExperience(this.uuidCurrentExperience);
   this.getChapterOptions(this.uuidCurrentChapter);
  }


  getChapterOptions(uuid: string): void{
    this.chapterOptionService.getChapterOptionByChapterID(uuid)
    .subscribe(
      data => {
        this.chapterOptionsList = data;
        console.log("chapterOptions:");
        console.log(data);
        console.log(this.chapterOptionsList);
      },
      error => {
        console.log(error);
      }
    );
  }

  getExperience(uuid: string): void{
    this.experienceService.getExperienceByID(uuid)
    .subscribe(
      data => {
        this.currentExperience = data;
        this.chaptersExp = this.currentExperience.chapters;
        const result = this.chaptersExp.find(({uuid}) => uuid === this.uuidCurrentChapter);
        console.log(result);
        this.buttons = result.buttons;
        console.log(this.buttons);
      },
      error => {
        console.log(error);
      }
    );
  }

  getChapter(uuid: string): void{
    this.chapterService.getChapterById(uuid)
    .subscribe(
      data => {
        this.currentChapter = data;
      },
      error => {
        console.log(error);
      }
    );
  }

  addButton(): void {
    const addExperienceData = {
      chapters: this.experience.chapters,
    };

    const addButtonData = {
      alias: this.button.alias,
      autoOpen: this.button.autoOpen,
      buttonType: this.button.buttonType,
      content: this.button.content,
      location: this.button.location,
      order: this.button.order,
      optionType: this.button.optionType,
      rotation: this.button.rotation,
      teleport: this.button.teleport,
      visibility: this.button.visibility
    };

    const addContentData = {
      key: this.content.key,
      version: this.content.version,
      alias: this.content.alias,
      value: this.content.value,
      type: this.content.type,
      quality: this.content.quality,
      language: this.content.language,
      visibility: this.content.visibility
    };

    const locationObject = new Location();
    locationObject.x = this.location.x;
    locationObject.y = this.location.y;
    locationObject.z = this.location.z;
    locationObject.alias = this.location.alias;
    this.locations.push(locationObject);

    const rotationObject = new Rotation();
    rotationObject.x = this.rotation.x;
    rotationObject.y = this.rotation.y;
    rotationObject.z = this.rotation.z;
    rotationObject.alias = this.rotation.alias;
    this.locations.push(rotationObject);

    if(this.buttonTypeText == "3"){
      const teleportButton = new ButtonTeleport();
      teleportButton.targetType = this.teleport.targetType;
      teleportButton.teleportTarget = this.teleportToText;
    }
    else{
      addButtonData.teleport = null;
    }



    console.log("experienceData")
    console.log(addExperienceData);


    const result = this.currentExperience.chapters.find(({uuid}) => uuid === this.uuidCurrentChapter);
    console.log(result);
    this.buttons = result.chapterOptions;

    this.buttons.push(addButtonData);

    result.buttons = this.buttons;
    this.currentChapter = result;

    console.log(addExperienceData.chapters);
    addExperienceData.chapters.push(this.currentChapter);
    console.log(addExperienceData.chapters);

    this.currentExperience.chapters = addExperienceData.chapters;

    this.experienceService.editExperience(this.currentExperience.uuid, this.currentExperience)
    .subscribe(res => {
      console.log(JSON.stringify(this.currentExperience));
      console.log(res);
      this.router.navigate['/detail-chapter/' + this.uuidCurrentExperience +'/'+ this.uuidCurrentChapter];
    },
      error => {
        console.log(error);
      }
    );
}

textBoxActive(){
  this.showTextBoxChangeVars =  false;
  this.showTextBoxContent = false;
  this.showTextBoxSwitch = false;
  this.showTextBoxTeleport = false;

  if (this.buttonTypeText == '0'){
    this.showTextBoxChangeVars =  false;
    this.showTextBoxSwitch = false;
    this.showTextBoxTeleport = false;
    this.showTextBoxContent = !this.showTextBoxContent;
  }
  if (this.buttonTypeText == '1'){
    this.showTextBoxChangeVars =  false;
    this.showTextBoxContent = false;
    this.showTextBoxSwitch = false;
    this.showTextBoxTeleport = !this.showTextBoxTeleport;
  }
  if(this.buttonTypeText == '2'){
    this.showTextBoxContent = false;
    this.showTextBoxSwitch = false;
    this.showTextBoxTeleport = false;
    this.showTextBoxChangeVars = !this.showTextBoxChangeVars;
  }
  if (this.buttonTypeText == '3'){
    this.showTextBoxChangeVars =  false;
    this.showTextBoxContent = false;
    this.showTextBoxTeleport = false;
    this.showTextBoxSwitch = !this.showTextBoxSwitch;
  }
}
onSelectTargetType(event){
  this.teleport.targetType =  event.target.value;
  console.log(this.teleport.targetType);
}

onSelectTeleportTo(event){
  this.teleportToText =  event.target.value;
  console.log(this.teleportToText);
}

onSelectVisibilityContent(event){
  this.button.visibility = event.target.value;
  this.visibilityButton = this.content.visibility;
}

onSelectVisibilityButton(event){
  this.content.visibility = event.target.value;
  this.visibilityContent = this.button.visibility;
}

onSelectButtonType(event){
this.button.buttonType = event.target.value;
this.buttonTypeText = this.button.buttonType;
console.log(this.buttonTypeText);
this.showTextBoxTeleport = true;
}



onSelectQuality(event){
  this.content.quality = event.target.value;
  this.contents.filter(element => {
    if (element.quality == event.target.value){
      this.qualityContent = element.quality;
    }
  });
}

onSelectType(event){
  this.content.type = event.target.value;
  this.contents.filter(element => {
    if (element.type == event.target.value){
      this.typeContent = element.type;
    }
  });

}
onSelected(event) {
  this.selectedFile = (event.target.files[0] as File);
  const reader = new FileReader();
  reader.readAsDataURL(this.selectedFile);
  reader.onload = (_event) => {
    this.url = reader.result;
  };
  console.log(this.selectedFile);
  console.log(this.url);
}

  logout(){
    this.authService.logout();
    this.router.navigate(['/login']);
}

  createContent(): void{

  const addContentData = {
  key: this.content.key,
  version: this.content.version,
  alias: this.content.alias,
  value: this.content.value,
  type: this.content.type,
  quality: this.content.quality,
  language: this.content.language,
  visibility: this.content.visibility
  };

  const result = this.chapterOptionsList.find(({uuid}) => uuid === this.selectedChapterOptionUuid);
  console.log(result);

  console.log(result.optionType);
  this.optionTypeValue = result.optionType;

  addContentData.quality = this.qualityContent;
  addContentData.language = this.languageContent;

 /* var contentClass = new Content();
  contentClass.uuid  = this.uuidContent;

  if(this.selectedFile == null ){
    addContentData.value = this.content.value;
   // addContentData.value = null;
  }
  else {
    addContentData.value = this.selectedFile.name;
  }*/

 /* this.contentService.addContentByItemId(this.selectedChapterOptionUuid, addContentData)

    .subscribe(data => {
      var cD = data as Content;
      console.log(data);
      console.log(this.selectedFile);
      if(this.typeContent == "0" || this.selectedFile == null){ // als text gekozen is en geen foto is, value is value
        console.log('you did it');
         // addContentData.value = this.content.value;
      }
      else{
        data.value = this.selectedFile.name;
        console.log("dedeefe");
        console.log(this.selectedFile);
        this.contentService.upload( data.uuid, this.selectedChapterOptionUuid, this.selectedFile)
      .subscribe(res => {
        console.log(res);

        this.router.navigate(['/list-experience']);

    });
      }
    },
      error => {
        console.log(error);
    }
);*/

  console.log(addContentData);
}

}
