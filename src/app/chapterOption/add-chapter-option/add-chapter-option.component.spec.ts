import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddChapterOptionComponent } from './add-chapter-option.component';

describe('AddChapterOptionComponent', () => {
  let component: AddChapterOptionComponent;
  let fixture: ComponentFixture<AddChapterOptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddChapterOptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddChapterOptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
