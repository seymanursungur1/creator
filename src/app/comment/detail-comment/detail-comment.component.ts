import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { CommentService } from 'src/app/service/comment.service';
import { UserService } from 'src/app/service/user.service';
import {Comment} from 'src/app/_models/comment';
import { User } from 'src/app/_models/user';
import { Location } from 'src/app/_models/location';
import { ContentService } from 'src/app/service/content.service';

@Component({
  selector: 'app-detail-comment',
  templateUrl: './detail-comment.component.html',
  styleUrls: ['./detail-comment.component.css']
})
export class DetailCommentComponent implements OnInit {

  currentComment: Comment;
  currentAuthor: string;
  contents = [];
  currentUser: User;
  authors = [];
  selectedUserUuid: string;
  uuid: string;
  users = [];
  constructor(private commentService: CommentService,
              private route: ActivatedRoute,
              private userService: UserService,
              private authService: AuthenticationService,
              private router: Router,
              private contentService: ContentService) {
                this.currentComment = new Comment();
                this.currentComment.location = new Location();
                this.currentUser = this.authService.currentUserValue;
                this.currentComment.author = new User();
              }

  ngOnInit(): void {
    this.uuid = this.route.snapshot.paramMap.get('uuid');
    console.log(this.uuid);
    this.getComment(this.uuid);
    this.getContent(this.uuid);

    this.userService.getAllUsers().subscribe((data: any[]) => {
      console.log(data);
      this.users = data;
    });
  }

  getContent(uuid: string): void{
    this.contentService.getContentByItemID(uuid)
    .subscribe(
      data => {
        this.contents = data;
       console.log("contents:")
       // console.log(data);
      },
      error => {
        console.log(error);
      }
    );
  }

  getComment(uuid: string): void{
    this.commentService.getCommentByID(uuid)
    .subscribe(
      data => {
        this.currentComment = data;
        console.log(this.currentComment);
        this.currentAuthor = this.currentComment.author.firstName;
      },
      error => {
        console.log(error);
      }
    );
  }

  detailContent(uuid): void{
    this.router.navigate(['detail-content/', uuid]);
  }
  deleteContent(uuid: string): void{
    this.contentService.deleteContent(uuid)
    .subscribe(
    response => {
      console.log(response);
      window.location.reload();
    },
    error => {
    console.error();
    });
    }

  editComment(){

      this.commentService.editComment(this.currentComment.uuid, this.currentComment)
      .pipe(first())
      .subscribe(res => {
           console.log(res);
           window.location.reload();
          },    error => {
            console.log(error);
          });
  }


onSelectUser(event){
  // this.user.uuid = event.target.value;
   this.selectedUserUuid = event.target.value;
   console.log(this.selectedUserUuid);
 }

 logout(){
  this.authService.logout();
  this.router.navigate(['/login']);
  }
}
