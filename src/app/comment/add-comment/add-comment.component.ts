import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { CommentService } from 'src/app/service/comment.service';
import { UserService } from 'src/app/service/user.service';
import { User } from 'src/app/_models/user';

@Component({
  selector: 'app-add-comment',
  templateUrl: './add-comment.component.html',
  styleUrls: ['./add-comment.component.css']
})
export class AddCommentComponent implements OnInit {

  location = {
    alias: '',
    x: 0,
    y: 0,
    z: 0
  };

  rotation = {
    alias: '',
    x: 0,
    y: 0,
    z: 0
  };
  comments = [];
  comment = {
    author: [],
    createDate: '',
    size: null,
    location: this.location,
    rotation: this.rotation,
    content: [],
    alias: ''
  };

  user: User;
  currentUser : User;
  users = [];
  uuidCurrentExperience: string;
  uuidCurrentChapter: string;
  selectedUserUuid: string;

  constructor(private commentService: CommentService,
              private authService: AuthenticationService,
              private router: Router,
              private route: ActivatedRoute,
              private userService: UserService) { this.currentUser = this.authService.currentUserValue; }

  ngOnInit(): void {
    this.uuidCurrentExperience = this.route.snapshot.paramMap.get('uuidExperience');
    // console.log(this.uuidCurrentExperience);
    this.uuidCurrentChapter = this.route.snapshot.paramMap.get('uuidChapter');

    this.userService.getAllUsers().subscribe((data: any[]) => {
      console.log(data);
      this.users = data;
    });
  }

  addComment(){

    const addCommentData = {
      author: this.comment.author,
      createDate: this.comment.createDate,
      size: this.comment.size,
      location: this.comment.location,
      rotation: this.comment.rotation,
      content: this.comment.content,
      alias: this.comment.alias
      };

    const result = this.users.find(({uuid}) => uuid === this.selectedUserUuid);
    console.log(result);
    addCommentData.author = result;
    addCommentData.size = 1;


    console.log(addCommentData);


    this.commentService.addCommentByChapterId(this.uuidCurrentChapter, addCommentData)
    .pipe(first())
    .subscribe(data => {
      this.router.navigate(['/detail-chapter/' + this.uuidCurrentExperience + '/' + this.uuidCurrentChapter]);},
      error => {
        console.log(error);
      }
    );

}

onSelectUser(event){
 // this.user.uuid = event.target.value;
  this.selectedUserUuid = event.target.value;
  console.log(this.selectedUserUuid);
}

logout(){
  this.authService.logout();
  this.router.navigate(['/login']);
}
}
