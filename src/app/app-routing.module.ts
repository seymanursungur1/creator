import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddChapterComponent } from './chapter/add-chapter/add-chapter.component';
import { DetailChapterComponent } from './chapter/detail-chapter/detail-chapter.component';
import { AddChapterOptionComponent } from './chapterOption/add-chapter-option/add-chapter-option.component';
import { AddButtonComponent } from './chapterOption/add/add-button/add-button.component';
import { AddQuizComponent } from './chapterOption/add/add-quiz/add-quiz.component';
import { AddVariantSetComponent } from './chapterOption/add/add-variant-set/add-variant-set.component';
import { DetailChapterOptionComponent } from './chapterOption/detail-chapter-option/detail-chapter-option.component';
import { DetailButtonComponent } from './chapterOption/detail/detail-button/detail-button.component';
import { DetailQuizComponent } from './chapterOption/detail/detail-quiz/detail-quiz.component';
import { DetailVariantSetComponent } from './chapterOption/detail/detail-variant-set/detail-variant-set.component';
import { AddCommentComponent } from './comment/add-comment/add-comment.component';
import { DetailCommentComponent } from './comment/detail-comment/detail-comment.component';
import { AddContentComponent } from './content/add-content/add-content.component';
import { ContentChapterComponent } from './content/content-chapter/content-chapter.component';
import { ContentExperienceComponent } from './content/content-experience/content-experience.component';
import { DetailContentComponent } from './content/detail-content/detail-content.component';
import { ListContentComponent } from './content/list-content/list-content.component';
import { AddExperienceComponent } from './experience/add-experience/add-experience.component';
import { EditExperienceComponent } from './experience/edit-experience/edit-experience.component';
import { ListExperienceComponent } from './experience/list-experience/list-experience.component';
import { FormUploadComponent } from './form-upload/form-upload.component';
import { HomeComponent } from './home/home.component';
import { ListUploadComponent } from './list-upload/list-upload.component';
import { LoginComponent } from './login/login.component';
import { AddUserComponent } from './user/add-user/add-user.component';
import { ListUserComponent } from './user/list-user/list-user.component';
import { AuthGuard } from './_helpers';


const routes: Routes = [
  { path: '', component: LoginComponent, pathMatch: 'full'},
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'list-user', component: ListUserComponent, canActivate: [AuthGuard]},
  { path: 'add-user', component: AddUserComponent, canActivate: [AuthGuard] },
  { path: 'list-upload', component: ListUploadComponent },
  { path: 'form-upload', component: FormUploadComponent },
  { path: 'list-experience', component: ListExperienceComponent },
  { path: 'add-experience', component: AddExperienceComponent },
  { path: 'edit-experience/:uuid', component: EditExperienceComponent },
  { path: 'list-content', component: ListContentComponent },
  { path: 'add-content', component: AddContentComponent },
  { path: 'add-content-experience/:uuid', component: ContentExperienceComponent},
  { path: 'add-content-chapter/:uuid', component: ContentChapterComponent},
  { path: 'detail-content/:uuid', component: DetailContentComponent},
  { path: 'detail-chapter/:uuidExperience/:uuidChapter', component: DetailChapterComponent},
  { path: 'add-chapter/:uuidExperience', component: AddChapterComponent },
  { path: 'add-chapterOption/:uuidExperience/:uuidChapter', component: AddChapterOptionComponent},
  { path: 'add-button/:uuidExperience/:uuidChapter', component: AddButtonComponent},
  { path: 'add-quiz/:uuidExperience/:uuidChapter', component: AddQuizComponent},
  { path: 'add-variant-set/:uuidExperience/:uuidChapter', component: AddVariantSetComponent},
  { path: 'detail-chapterOption/:uuidChapterOption', component: DetailChapterOptionComponent},
  { path: 'detail-button/:uuidExperience/:uuidChapter/:uuidChapterOption', component: DetailButtonComponent},
  { path: 'detail-quiz/:uuidChapterOption', component: DetailQuizComponent},
  { path: 'detail-variantSet/:uuidChapter/:uuidChapterOption', component: DetailVariantSetComponent},
  { path: 'add-comment/:uuidExperience/:uuidChapter', component: AddCommentComponent},
  { path: 'detail-comment/:uuid', component: DetailCommentComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
