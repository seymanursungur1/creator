import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ListUserComponent } from './user/list-user/list-user.component';
import { AddUserComponent } from './user/add-user/add-user.component';
import { HttpClientModule} from '@angular/common/http';

import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListUploadComponent } from './list-upload/list-upload.component';
import { FormUploadComponent } from './form-upload/form-upload.component';
import { UploadFileService } from './service/upload-file.service';
import { DetailsUserComponent } from './user/details-user/details-user.component';
import { AddContentComponent } from './content/add-content/add-content.component';
import { ListContentComponent } from './content/list-content/list-content.component';
import { AddExperienceComponent } from './experience/add-experience/add-experience.component';
import { ListExperienceComponent } from './experience/list-experience/list-experience.component';
import { EditExperienceComponent } from './experience/edit-experience/edit-experience.component';
import { EnumToArrayPipePipe } from './pipes/enum-to-array-pipe.pipe';
import { DetailContentComponent } from './content/detail-content/detail-content.component';
import { DetailChapterComponent } from './chapter/detail-chapter/detail-chapter.component';
import { AddChapterComponent } from './chapter/add-chapter/add-chapter.component';
import {MatTabsModule} from '@angular/material/tabs';
import { AddChapterOptionComponent } from './chapterOption/add-chapter-option/add-chapter-option.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AddCommentComponent } from './comment/add-comment/add-comment.component';
import { DetailCommentComponent } from './comment/detail-comment/detail-comment.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { ContentQuizComponent } from './content/content-quiz/content-quiz.component';
import { ContentButtonComponent } from './content/content-button/content-button.component';
import { AddButtonComponent } from './chapterOption/add/add-button/add-button.component';
import { AddQuizComponent } from './chapterOption/add/add-quiz/add-quiz.component';
import { AddVariantSetComponent } from './chapterOption/add/add-variant-set/add-variant-set.component';
import { AddVariantComponent } from './chapterOption/add/add-variant/add-variant.component';
import { DetailChapterOptionComponent } from './chapterOption/detail-chapter-option/detail-chapter-option.component';
import { AddChapterOptionButtonComponent } from './chapterOption/add-chapter-option-button/add-chapter-option-button.component';
import { DetailButtonComponent } from './chapterOption/detail/detail-button/detail-button.component';
import { DetailQuizComponent } from './chapterOption/detail/detail-quiz/detail-quiz.component';
import { DetailVariantSetComponent } from './chapterOption/detail/detail-variant-set/detail-variant-set.component';
import { ContentExperienceComponent } from './content/content-experience/content-experience.component';
import { ContentChapterComponent } from './content/content-chapter/content-chapter.component';
@NgModule({
  declarations: [
    EnumToArrayPipePipe,
    AppComponent,
    HomeComponent,
    LoginComponent,
    ListUserComponent,
    AddUserComponent,
    DetailsUserComponent,
    ListUploadComponent,
    FormUploadComponent,
    AddContentComponent,
    ListContentComponent,
    AddExperienceComponent,
    ListExperienceComponent,
    EditExperienceComponent,
    DetailContentComponent,
    DetailChapterComponent,
    AddChapterComponent,
    AddChapterOptionComponent,
    AddCommentComponent,
    DetailCommentComponent,
    DetailChapterOptionComponent,
    ContentQuizComponent,
    ContentButtonComponent,
    AddChapterOptionButtonComponent,
    DetailButtonComponent,
    DetailQuizComponent,
    DetailVariantSetComponent,
    AddButtonComponent,
    AddQuizComponent,
    AddVariantSetComponent,
    AddVariantComponent,
    ContentExperienceComponent,
    ContentChapterComponent
  ],
  imports: [
    Ng2SearchPipeModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatButtonModule,
    MatTabsModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [UploadFileService],
  bootstrap: [AppComponent],
  entryComponents: [DetailsUserComponent, AddChapterComponent]
})
export class AppModule { }
